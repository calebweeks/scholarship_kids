
ALTER TABLE scholarship_kids drop  region_id;
drop table if exists region_scholarship_link;
drop table if exists regions;
create table regions(
	id varchar(2)  PRIMARY KEY, 
	name varchar(200), 
	currency varchar(3)
);	

insert into regions(id, name, currency) VALUES('us','United States', 'USD');
insert into regions(id, name, currency) VALUES('ca','Canada', 'CAD');

ALTER TABLE scholarship_kids ADD region_id varchar(2) DEFAULT "us"; 

create table region_scholarship_link(
	id int AUTO_INCREMENT PRIMARY KEY, 
	region_id varchar(2) not null, 
	scholarship_type_id int not null,
	amount int, 
	alternate_spelling varchar(200),
	FOREIGN KEY(scholarship_type_id) references scholarship_types(id),
	FOREIGN KEY(region_id) references regions(id)
); 

insert into region_scholarship_link(region_id, scholarship_type_id, amount)
VALUES('us', (select id from scholarship_types where type_name = 'Diploma Scholarship'), 840);

insert into region_scholarship_link(region_id, scholarship_type_id, amount)
VALUES('ca', (select id from scholarship_types where type_name = 'Diploma Scholarship'), 1080);

insert into region_scholarship_link(region_id, scholarship_type_id, amount)
VALUES('us', (select id from scholarship_types where type_name = 'Honor Roll Scholarship'), 420);

insert into region_scholarship_link(region_id, scholarship_type_id, amount, alternate_spelling)
VALUES('ca', (select id from scholarship_types where type_name = 'Honor Roll Scholarship'), 540, 'Honour Roll Scholarship');

SET SQL_SAFE_UPDATES=0;
update donation_frequency set display_template = concat(display_template, ' {currency}');

insert into scholarship_community(community_name) VALUES('Chimazat');
insert into scholarship_community(community_name) VALUES('Hacienda María');
insert into scholarship_community(community_name) VALUES('Las Camelias');
insert into scholarship_community(community_name) VALUES('Paley');
insert into scholarship_community(community_name) VALUES('Saquitacaj');




 