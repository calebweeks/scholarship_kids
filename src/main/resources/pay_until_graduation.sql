/* Until graduation changes */
SET SQL_SAFE_UPDATES = 0;

update donation_frequency set display_template = 'Monthly payment amount: ${amount}' 
where frequency_code = 'MONTHLY'; 

update donation_frequency set display_template = 'Annual payment amount: ${amount}' 
where frequency_code = 'ANNUALLY'; 

update donation_frequency set display_template = 'Single Payment Amount: ${amount}' 
where frequency_code = 'ONE_TIME'; 

update donation_frequency set label = "One year of {student.knownAs}'s scholarship" 
where frequency_code = 'ONE_TIME'; 

insert into donation_frequency(frequency_code, label, description, payment_factor, display_template, etapestry_value)
values('UNTIL_GRADUATION', 'Until {student.knownAs} graduates', 'Pay for the scholarship until the student graduates' ,1, 'Payment Amount: ${amount}', 0);

insert into additional_donation_mapping(scholarship_type_code, donation_frequency_code, default_amount, amounts)
values('HONOR', 'UNTIL_GRADUATION', 115, '[115, 550, 1050, 2500]'), 
('DIPLOMA', 'UNTIL_GRADUATION', 160, '[160, 550, 1050, 2500]');


select max(id) from scholarship_kids; 
#old: 5411
#new: 5761

select * from scholarship_community; 
#old: '71', 'Saquitacaj', NULL, NULL
#new: '121', 'Santo Domingo', '0', '0'


