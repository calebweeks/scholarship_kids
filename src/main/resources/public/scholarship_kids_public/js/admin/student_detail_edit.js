//find all of the delete scholarship buttons
$(document).on('pageinit',function(){
    $('.deleteConfirm').click(function(e){
        e.preventDefault();

        //find the id from the parent
        var endpoint = $(this).closest('.deleteScholarshipForm').data('endpoint');

        //execute the AJAX
        $.ajax({
            url: endpoint,
            method: 'DELETE'
        }).done(function(data){
            if(data.status === "success"){
                window.location = data.redirectTo;
            } else{
                COED.ACTIONS.popupMessage('There was an error updating the scholarship.<br/>' + data.error);
             }
        });
    }) ;


    //find all of the update buttons
    $('.updateConfirm').click(function(e){
        e.preventDefault();

        //find the id from the parent

        var formContainer = $(this).closest('.updateScholarshipForm'),
            endpoint = formContainer.data('endpoint'),
            data = {'sponsorName': formContainer.find('input[name="sponsorName"]').val(),
                    'status': formContainer.find('select[name="status"]').val()};



        //execute the AJAX
        $.ajax({
            url: endpoint,
            method: 'PUT',
            data: data
        }).done(function(data){
            if(data.status === "success"){
                window.location = data.redirectTo;
            } else{
                 COED.ACTIONS.popupMessage('There was an error updating the scholarship.<br/>' + data.error);
            }
        });
    }) ;

    //find the delete student button
    $('.deleteStudentConfirm').click(function(e){
        e.preventDefault();

        //find the id from the parent
        var endpoint = $(this).closest('.deleteStudentForm').data('endpoint');

        //execute the AJAX
        $.ajax({
            url: endpoint,
            method: 'DELETE'
        }).done(function(data){
            if(data.status === "success"){
                window.location = data.redirectTo;
            } else{
                COED.ACTIONS.popupMessage('There was an error deleting the student.<br/>' + data.error);
             }
        });
    }) ;

});