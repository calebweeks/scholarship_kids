COED = {

    ACTIONS: {
        popupMessage: function(message){
            var globalPopup = $( "#userMessagePopup" );
            globalPopup.find('p').html(message);
            globalPopup.popup( 'open' );
        }
    }
};

$(document).on('pageinit',function(){
    //capture all manual close buttons
    $('.manualClose').click(function(e){
        e.preventDefault();

        //grab the href, pull in the popup, and close
        $($(this).attr('href')).popup('close');
    });
       });


$(document).on('pageinit',function(){
    //check to see if we have a param called 'userMessage'
    //if so, pop up that message.
    var userMessage = $.url().param('userMessage');
    if(userMessage){
        COED.ACTIONS.popupMessage(userMessage);
    }
});


