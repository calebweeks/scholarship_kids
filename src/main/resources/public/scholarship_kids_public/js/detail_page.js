$(function(){

	var isCa = $('body').data('region') === 'ca';

	var getClientData = function(){
	    return $('#clientData').data('client-data');
	},
	getSelectedFrequencyPaymentFactor = function(){
	    var factor = $('#sponsorshipFreqId').find(':selected').data('payment-factor');
	    if(getSelectedFrequencyCode() === 'UNTIL_GRADUATION'){
	        //need to do some wonky division so that the end result is
	        //actually multiplied
            factor = 1 / (factor * getClientData().student.yearsRemaining);
	    }
	    return factor;
	},
	getSelectedFrequencyPaymentTemplate = function(){
	    return $('#sponsorshipFreqId').find(':selected').data('display-template');
	},
	getSelectedFrequencyCode = function(){
	    return $('#sponsorshipFreqId').find(':selected').data('frequency-code')
	},
	getSelectedFrequencyEtapestryValue = function(){
	    return $('#sponsorshipFreqId').find(':selected').data('etapestry-value')
	},
	getSelectedScholarshipValue = function(){
	    return $('#sponsorshipTypeId').find(':selected').data('amount');
	},
	getSelectedScholarshipType = function(){
	    return $('#sponsorshipTypeId').val();
	},
	getSelectedScholarshipCode = function(){
	    return  $('#sponsorshipTypeId').find(':selected').data('type-code');
	},
	getAdditionalDonationMapping = function(){
	    return $('#additionalDonationsAmount').data('additional-donation-mapping');
	},
	getAdditionDonationAmount = function(){
	    return $('#wantToDonateAdditionalAmount').is(':checked') ? parseInt($('#additionalDonationsAmount').find(':selected').val()) : 0;
	},
	getTotalDonationAmount = function(){
	    return (getSelectedScholarshipValue() / getSelectedFrequencyPaymentFactor()) + getAdditionDonationAmount();
	},
	processTemplate = function(template, params, paramNamesToReplace){
	    //default list
	    /* TODO: this whold method of processing templates is not very good
	        instead either parse out the template variables first (ie find all {anything} and make
	        that be the list of variables. Or just use a real templating system...probably the best
	        idea. Do some more looking on that when you get internet again. */
	    var PARAM_NAMES_TO_REPLACE = ['amount', 'currency','student.knownAs'],

	    getParamValue = function(params, rootParamName){
	        //split the param name on '.'
	        var splitParamNames = rootParamName.split('.');

	        //iterate through the param names and get the value from the param
	        //for the current obj
	        var curParamObj = params;
	        $.each(splitParamNames, function(index, paramName){
	            if(curParamObj){
	                curParamObj = curParamObj[paramName];
                }
	        });

	        return curParamObj;
	    };

	    paramNamesToReplace = paramNamesToReplace || PARAM_NAMES_TO_REPLACE;

        //replace each of the params in the template
        $.each(paramNamesToReplace, function(index, paramName){
            template = template.replace('{' + paramName + '}', getParamValue(params, paramName));
        });

        return template;

	},
	updateChosenSelect = function(select){
	    select.trigger('liszt:updated');
	},
	updateScholarshipValueFields = function(){
		var paymentTemplate = getSelectedFrequencyPaymentTemplate(),
		paymentAmount = getTotalDonationAmount();

		//update hidden fields and ui
        $('#scholarshipAmountId').val(paymentAmount);

				var displayElement = $('.scholarship_form .scholarship-value-info .payment-amt');
        //process the template
        var displayString = processTemplate(paymentTemplate, {'amount' : paymentAmount, 'currency' : displayElement.data('currency')});

        //display the processed template
        displayElement.html(displayString);

        //update the rgsFrequency field
        $('#rgsFrequency').val(getSelectedFrequencyEtapestryValue());
	},
    populateAdditionalDonationValues = function(){
	    var typeCode = getSelectedScholarshipCode(),
            frequencyCode = getSelectedFrequencyCode(),
            additionDonationsSelectBox = $('#additionalDonationsAmount'),
            additionalDonationMapping = getAdditionalDonationMapping();

	    //get the additional donation values for the current set
        if(additionalDonationMapping && additionalDonationMapping[typeCode] && additionalDonationMapping[typeCode][frequencyCode]){
            var donationOptions = additionalDonationMapping[typeCode][frequencyCode];

            //add each of the values as options
            additionDonationsSelectBox.html(''); //remove any options that may be there
            $.each(donationOptions.values, function(index, value){
                additionDonationsSelectBox.append($("<option></option>")
                         .attr("value",value)
                         .text('$' + value + ' ' + additionDonationsSelectBox.data('currency'))
                         .prop("selected", (value === donationOptions.defaultVal)));
            });

            //update chosen
            updateChosenSelect(additionDonationsSelectBox);
        }
	},
    processInitialPageTemplates = function(){
        var templateParameters = $.extend(
            {
                'amount': getTotalDonationAmount()
            },
            getClientData());

        //find frequency options and process names
        $('#sponsorshipFreqId option').each(function(){
            var curOption = $(this);

            //run the text through the template processor and reassign
            var processedText = processTemplate(curOption.text(), templateParameters);
            curOption.text(processedText);

        });

        //update chosen
        updateChosenSelect($('#sponsorshipFreqId'));

    },
    showCorrectSponsorLinkForCa = function(){
			//hide all the sponsor buttons
			$('.sponsor_button_container a').hide();

			//show the correct one
			$('.sponsor_button_container a.' + getSelectedScholarshipCode()).show();
    },
	bindEventListeners = function(){

	    //add listener to sponsor button - simply submit the form
	    if(!isCa){
        $('.scholarship_form .sponsor_button_container .action-btn').click(function(event){
            event.preventDefault();
            event.stopPropagation();

            //submit the form
            $(this).parents('form.scholarship_form').submit();
        });

	    	//add listener to selection of scholarship to update form fields
        //only change the scholarship type or frequency should change the additional
        //donation value dropdown, but changing that dropdown or clicking the checkbox
        //should also update the total value.
        //Using chosen change listener rather than the Jquery change listener as
        //some browser won't fire that event if the select has the same value after
        //the change
        $('#sponsorshipTypeId, #sponsorshipFreqId').chosen().change(populateAdditionalDonationValues);
        $('#sponsorshipTypeId, #sponsorshipFreqId, #additionalDonationsAmount').chosen().change(updateScholarshipValueFields);
        $('#wantToDonateAdditionalAmount').change(updateScholarshipValueFields);
			}else{
				$('#sponsorshipTypeId').chosen().change(showCorrectSponsorLinkForCa);
			}

	},
	init = function(){
	    //initialize fancy select boxes
			$('.scholarship_form select').chosen();


	    //initialize the scholarship field values
	    if(!isCa){
				populateAdditionalDonationValues();
				updateScholarshipValueFields();
				processInitialPageTemplates();
			}else{
				showCorrectSponsorLinkForCa();
			}


			 //bind event listeners
			bindEventListeners();
	};


	//kick off init
	init();

});