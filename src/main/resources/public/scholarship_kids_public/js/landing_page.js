$(function(){
	//some vars that need to be accessible on a larger scope
	var landingPageContainer = $('.landing_page_container'),
		resultsContainer = landingPageContainer.find('.results_container'),
		studentsContainer =  resultsContainer.find('.students_container'),
		students = studentsContainer.find('.student'), 
		paginationContainers = resultsContainer.find('.pagination'),
		initialPaginationStateHandled= !window.location.hash; //we only need to care about the page state if there is hash present
	
	function _normalizeHeight(){
		//landing page 
		var studentSummaries = students.find('.student_summary');
		
		
		
		//find student with the largest height
		var largestHeight;
		studentSummaries.each(function(){
			var student = $(this);
			if(!largestHeight || largestHeight < student.height()){
				largestHeight = student.height();
			}
		});
		
		//set the height of all the students
		studentSummaries.height(largestHeight);
		
	}; 
	
	//invokes the chosen plugin on select boxes
	function _initSearchBar(){
		var searchFields = $('.search_container select');
		searchFields.chosen({allow_single_deselect: true});
		
		//handle click on search button
		$('.search_container .action-btn').click(function(event){
			event.preventDefault();
			event.stopPropagation();
			
			//add loading icon
			//resultsContainer.addClass("loading");
			
			//function that returns true if the given criteria match
			var matchesCriteria = function(searchFieldValue, eligibility){
				fitsCriteria = true; 
				
				//only care about this criteria
				//if a value was set
				if(searchFieldValue){
					//if we've got an array, see if the value of the
					//field is contained
					if(eligibility instanceof Array){
						fitsCriteria = ($.inArray(searchFieldValue, eligibility) > -1);
					}else {
						//otherwise, just do a check on the string value
						fitsCriteria = (searchFieldValue === eligibility.toString());
					}
					
				}
				
				return fitsCriteria; 
			};
			//iterate through all students and see and check if each 
			//is eligible given the current criteria. if it's not, hide it
			students.each(function(){
				var student = $(this),
					eligibility = student.data('filter-eligibility'),
					fitsCriteria = true; 
				
				//iterate through each value submitted
				//and check with corresponding eligibility
				searchFields.each(function(){
					var searchField = $(this);
					fitsCriteria &= matchesCriteria(searchField.val(), eligibility[searchField.attr('name')]);
				});
				
				
				
				//show or hide, depending on fit
				if(fitsCriteria){
					student.show();
					student.addClass('match-criteria');
				}else{
					student.hide();
					student.removeClass('match-criteria')
				}
			});
			
			
			//identify how many students are or will be showing
			var visibleStudents = students.filter('.match-criteria');
			
			//adjust ui as needed based on number visible
			if(visibleStudents.length){
				resultsContainer.removeClass('no-results');
			}else{
				resultsContainer.addClass('no-results');
			}
			
			//redefine pagination
			paginationContainers.pagination({
				items: visibleStudents.length, 
				itemsOnPage: 4,
				currentPage: 1,
				cssStyle: 'light-theme', 
				onPageClick: function(pageNumber){
					//pageNumber starts at 1. so subtract first
					var firstItemShown = (pageNumber - 1) * 4, 
						lastItemShown = firstItemShown + (visibleStudents.length - firstItemShown < 4 ? visibleStudents.length - firstItemShown : 4);
					
					//update item count text
					paginationContainers.siblings('.item_count').html((firstItemShown + 1) + ' - ' + (lastItemShown) + ' of ' + visibleStudents.length)
					//iterate through each student and hide any
					//that are not in the currently visible range
					visibleStudents.each(function(index){
						if((index >= firstItemShown) && (index < (firstItemShown + 4))){
							$(this).slideDown();
						}else{
							$(this).slideUp();
						}
					});
					
				}, 
				onInit: function(){
					paginationContainers.pagination('selectPage', 1);
				}
				
			
			});
			
			//remove loading icon
			//removing for now
			//resultsContainer.removeClass("loading");
		});
		
		//click, just in case some values have been prepopulated
		$('.search_container .action-btn').click();
		
		//determine what page should be selected
		var selectedPage = 1; 
		if(!initialPaginationStateHandled){
			//attempt to pull in the value from the hash
			var hash = window.location.hash;
			
			if(hash.indexOf('#page-') === 0){
				selectedPage = hash.substring(6);
				
				if(selectedPage < 1 || selectedPage > (paginationContainers.pagination('getPagesCount'))){
					selectedPage = 1; 
				}
			}
			
			
			initialPaginationStateHandled = true;
		}
		paginationContainers.pagination('selectPage', selectedPage);
		
	}
	
	//execute functions
	_initSearchBar();
	_normalizeHeight();
	
	//hide the loading icon and display everything
	landingPageContainer.removeClass('initializing');
});
	
