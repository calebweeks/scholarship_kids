/* Clear out table for starting fresh */
SET SQL_SAFE_UPDATES = 0;
drop table if exists additional_donation_mapping; 
drop table if exists donation_frequency;

alter table scholarship_types 
add column type_code varchar(50) unique; 

-- alter table scholarship_types 
-- drop column type_code; 

update scholarship_types set type_code = 'HONOR' where type_name = 'Honor Roll Scholarship'; 
update scholarship_types set type_code = 'DIPLOMA' where type_name = 'Diploma Scholarship';

create table donation_frequency
(id int auto_increment primary key, 
frequency_code varchar(50) not null unique, 
label varchar(200), 
description text, 
payment_factor int not null, 
display_template varchar(500) not null, 
etapestry_value varchar(50) not null
); 

insert into donation_frequency (label, frequency_code, description, payment_factor, display_template, etapestry_value) 
values('Monthly', 'MONTHLY', 'The sponsor makes a donation every month until the scholarship is fulfilled.', 12, 'Monthly payment amount: {amount}', '12'), 
('Annually', 'ANNUALLY', 'The sponsor makes a donation once a year until the scholarship is fulfilled.', 1, 'Annual payment amount: {amount}', '1'),
('One-Time', 'ONE_TIME', 'The entirety of the scholarship is paid at one time.', 1, 'Single Payment Amount: {amount}', '0'); 


create table additional_donation_mapping
(
id int auto_increment primary key, 
scholarship_type_code varchar(50) not null, 
donation_frequency_code varchar(50) not null, 
default_amount int not null, 
amounts varchar(500), 
foreign key(scholarship_type_code) references scholarship_types(type_code), 
foreign key(donation_frequency_code) references donation_frequency(frequency_code)

);

select * from donation_frequency; 
select * from scholarship_types;

insert into additional_donation_mapping (scholarship_type_code, donation_frequency_code, default_amount, amounts) 
values('HONOR', 'MONTHLY', 15, '[5,15,25,30]'),
('HONOR', 'ANNUALLY', 30, '[30,60,85,115]'), 
('HONOR', 'ONE_TIME', 30, '[30,60,85,115]'), 
('DIPLOMA', 'MONTHLY', 30, '[15,25,30,65]'), 
('DIPLOMA', 'ANNUALLY', 160, '[40,90,160,200]'), 
('DIPLOMA', 'ONE_TIME', 160, '[40,90,160,200]');


