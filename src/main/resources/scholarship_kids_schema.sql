
DROP TABLE IF EXISTS kid_scholarship_link;
DROP TABLE IF EXISTS scholarship_kids; 
DROP TABLE IF EXISTS scholarship_community;
DROP TABLE IF EXISTS scholarship_types; 
DROP TABLE IF EXISTS school_levels; 


CREATE TABLE scholarship_community(
	id int AUTO_INCREMENT PRIMARY KEY, 
	community_name varchar(200) NOT NULL, 
	community_latitude double, 
	community_longitude double
	
);

CREATE TABLE school_levels(
	id int AUTO_INCREMENT PRIMARY KEY, 
	level_name varchar(200)
);

CREATE TABLE scholarship_types(
	id int AUTO_INCREMENT PRIMARY KEY, 
	amount int, 
	type_name varchar(200), 
	description text, 
	etapestry_value varchar(200) NOT NULL
); 
CREATE TABLE scholarship_kids(
	id int AUTO_INCREMENT PRIMARY KEY, 
	first_name varchar(200) NOT NULL, 
	last_name varchar(400) NOT NULL, 
	known_as varchar(200), 
	gender char NOT NULL, 
    birthdate datetime NOT NULL, 
	image_path varchar(400), 
	grade int,
	biography text, 
	future_aspirations text, 
	misc_notes text,
	fully_sponsored char default 'n',
	favorite_subject varchar(200),
	community_id int,
	school_level_id int, 
	etapestry_account_number int, 
	sponsor_year int,
	date_created timestamp NOT NULL default CURRENT_TIMESTAMP, 
	FOREIGN KEY(community_id) references scholarship_community(id),
	FOREIGN KEY(school_level_id) references school_levels(id)
	
); 
	

CREATE TABLE kid_scholarship_link(
	id int AUTO_INCREMENT PRIMARY KEY, 
	scholarship_type_id int not null, 
	scholarship_kid_id int not null,
	scholarship_status varchar(10) default 'available', 
	scholarship_sponsor_name varchar(200),
	FOREIGN KEY(scholarship_type_id) references scholarship_types(id),
	FOREIGN KEY(scholarship_kid_id) references scholarship_kids(id)
);

INSERT INTO scholarship_community(community_name, community_latitude, community_longitude) VALUES("Santiago Sacatepéquez",14.638607,-90.679693);
INSERT INTO scholarship_community(community_name, community_latitude, community_longitude) VALUES("Patzicía",14.6317,90.9269); 
INSERT INTO scholarship_community(community_name, community_latitude, community_longitude) VALUES("Patzún",14.685232,-91.012888); 

INSERT INTO school_levels(level_name) VALUES("High School"); 
INSERT INTO school_levels(level_name) VALUES("Middle School");

INSERT INTO scholarship_types(amount, type_name, description, etapestry_value) VALUES(420, "Honor Roll Scholarship", "Sponsor half of the student’s cost.", "Honor Roll Scholarship");
INSERT INTO scholarship_types(amount, type_name, description, etapestry_value) VALUES(840, "Diploma Scholarship", "Completely sponsor all of the student's cost.", "Diploma Scholarship");
