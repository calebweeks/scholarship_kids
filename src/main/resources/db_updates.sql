select * from kid_scholarship_link where scholarship_kid_id = 641;
select Distinct scholarship_status from kid_scholarship_link;
select * from scholarship_types;

/** grant dimploma scholarship */ 
update scholarship_kids set fully_sponsored = 'y' where id = 481; 
delete from kid_scholarship_link where scholarship_type_id = 1 and scholarship_kid_id = 481; 
update kid_scholarship_link set scholarship_status = 'sponsored', scholarship_sponsor_name = 'Unknown' where scholarship_type_id = 11 and scholarship_kid_id = 481; 
 

/** grant first honor scholarship*/
delete from kid_scholarship_link where scholarship_type_id = 11 and scholarship_kid_id = 71; 
update kid_scholarship_link set scholarship_status = 'sponsored', scholarship_sponsor_name = 'Unknown' where scholarship_type_id = 1 and scholarship_kid_id = 71; 
insert into kid_scholarship_link (scholarship_kid_id, scholarship_type_id) Values(71, 1); 

/** grant second honor scholarship*/
update scholarship_kids set fully_sponsored = 'y' where id = 561; 
update kid_scholarship_link set scholarship_status = 'sponsored', scholarship_sponsor_name = 'Unknown' where scholarship_status = 'available' and  scholarship_type_id = 1 and scholarship_kid_id = 561; 



update scholarship_kids set fully_sponsored = 'r' where id = 331; 
delete from kid_scholarship_link where id = 1361; 
delete from kid_scholarship_link where id = 391;
update scholarship_kids set misc_notes = "Removed from scholarship program." where id = 331;

/*'1', '420', 'Honor Roll Scholarship', 'Sponsor half of the student’s cost.', 'Honor Roll Scholarship'
'11', '840', 'Diploma Scholarship', 'Completely sponsor all of the student\'s cost.', 'Diploma Scholarship'*/

