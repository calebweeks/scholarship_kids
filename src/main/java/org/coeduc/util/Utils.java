package org.coeduc.util;

import com.google.gson.JsonObject;
import spark.Response;

/**
 * Created by calebweeks on 2/9/14.
 */
public class Utils {


    /**
     * Encapsulates everything needed for modifying the response to return json
     * @param json
     * @param response
     * @return
     */
    public static String sendJsonRespsonse(JsonObject json, Response response){
        response.raw().setContentType("application/json");
        response.status(200);

        return json.toString();
    }
}
