/**
 * 
 */
package org.coeduc.util;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * @author calebweeks
 *
 */
public class ErrorUtility {
	public static String getStackTraceAsString(Exception e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}
	public static String getStackTraceAsHtmlString(Exception e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		String stackTrace = sw.toString(); 
		stackTrace = stackTrace.replaceAll("\n", "<br/>").replaceAll("\t", "<span style=\"margin-left:20px;\"></span>");
		return stackTrace;
	}
}
