/**
 * 
 */
package org.coeduc;

/**
 * @author calebweeks
 *
 */
public class Constants {
	public static final String FRONT_END_TEMPLATES_ROOT = "templates/front_end/";
	public static final String BACK_END_TEMPLATES_ROOT = "templates/administration/";

    public static final String STUDENTS_TEMPLATE_PATH = Constants.BACK_END_TEMPLATES_ROOT + "students/students.vm";
    public static final String STUDENT_DETAIL_TEMPLATE_PATH = Constants.BACK_END_TEMPLATES_ROOT + "students/student_detail.vm";
    public static final String STUDENT_DETAIL_EDIT_TEMPLATE_PATH = Constants.BACK_END_TEMPLATES_ROOT + "students/student_detail_edit.vm";
    public static final String ADMIN_ERROR_TEMPLATE_PATH = Constants.BACK_END_TEMPLATES_ROOT + "error.vm";
    public static final String ADMIN_LOGIN_TEMPLATE_PATH = Constants.BACK_END_TEMPLATES_ROOT + "login.vm";

    public static final String SCHOLARSHIP_KIDS_TEMPLATE = FRONT_END_TEMPLATES_ROOT + "common/main_template.vm";

}
