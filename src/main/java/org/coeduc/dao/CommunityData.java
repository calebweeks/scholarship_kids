/**
 * 
 */
package org.coeduc.dao;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author calebweeks
 *
 */
@DatabaseTable(tableName="scholarship_community")
public class CommunityData implements Comparable<CommunityData>{
	
	@DatabaseField(generatedId=true)
	private int id; 
	
	@DatabaseField(columnName="community_name")
	private String name; 
	
	@DatabaseField(columnName="community_latitude")
	private double latitude; 
	
	@DatabaseField(columnName="community_longitude")
	private double longitude; 
	
	
	public CommunityData(){
		
	}
	
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the latitude
	 */
	public double getLatitude() {
		return latitude;
	}


	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}


	/**
	 * @return the longitude
	 */
	public double getLongitude() {
		return longitude;
	}


	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
	@Override 
	public int hashCode(){
		return name != null ? name.hashCode() : id;
	}
	
	@Override
	public boolean equals(Object o){
		return o instanceof CommunityData &&
			   ((CommunityData)o).id == id;
	}


	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(CommunityData o) {
		return name != null ? name.compareTo(o.getName()) : id - o.getId();
	}

}
