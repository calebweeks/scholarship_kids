package org.coeduc.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author calebweeks
 *
 *
 *This class represents the many to many relationship linking table for
 *regions and scholarships
 */
@DatabaseTable(tableName="region_scholarship_link")
public class RegionScholarshipLinkData {

	@DatabaseField(generatedId=true)
	private int id;

	@DatabaseField
	private int amount;

	@DatabaseField(columnName = "alternate_spelling")
	private String alternateSpelling;

	@DatabaseField(columnName = "scholarship_type_id", foreign=true, foreignAutoRefresh=true)
	private ScholarshipTypeData scholarship;

	@DatabaseField(columnName = "region_id", foreign=true, foreignAutoRefresh=true)
	private RegionData region;

	public RegionScholarshipLinkData(){}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public RegionData getRegion() {
		return region;
	}

	public ScholarshipTypeData getScholarship() {
		return scholarship;
	}

	public String getAlternateSpelling() {
		return alternateSpelling;
	}
}
