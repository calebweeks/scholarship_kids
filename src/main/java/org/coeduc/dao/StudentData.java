/**
 * 
 */
package org.coeduc.dao;

import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.coeduc.data.adapters.HiddenFrontEnd;
import org.coeduc.data.adapters.ReadOnlyFrontEnd;
import org.coeduc.data.adapters.StudentDataFieldAdapter;
import org.coeduc.data.fields.*;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author calebweeks
 *
 */
@DatabaseTable(tableName="scholarship_kids")
public class StudentData {
	private static Map<String, StudentDataFieldAdapter> FIELD_TRANFORMER_MAPPING = new HashMap<String, StudentDataFieldAdapter>();
	
	static{
		FIELD_TRANFORMER_MAPPING.put("id", new StudentDataFieldAdapter("ID", "text"));
		FIELD_TRANFORMER_MAPPING.put("firstName", new StudentDataFieldAdapter("First Name", "text"));
		FIELD_TRANFORMER_MAPPING.put("lastName", new StudentDataFieldAdapter("Last Name", "text"));
		FIELD_TRANFORMER_MAPPING.put("knownAs", new StudentDataFieldAdapter("Known As", "text"));
		FIELD_TRANFORMER_MAPPING.put("gender", new StudentDataFieldAdapter("Gender", "select", new GenderTransformer()));
		FIELD_TRANFORMER_MAPPING.put("birthDate", new StudentDataFieldAdapter("Birth Date", "date", new DateTranformer()));
		FIELD_TRANFORMER_MAPPING.put("dateCreated", new StudentDataFieldAdapter("Date Added", "date", new DateTranformer()));
		FIELD_TRANFORMER_MAPPING.put("imagePath", new StudentDataFieldAdapter("Image Path", "text"));
		FIELD_TRANFORMER_MAPPING.put("grade", new StudentDataFieldAdapter("Grade", "select"));
		FIELD_TRANFORMER_MAPPING.put("biography", new StudentDataFieldAdapter("Biography", "textarea"));
		FIELD_TRANFORMER_MAPPING.put("futureAspirations", new StudentDataFieldAdapter("Future Aspirations", "textarea"));
		FIELD_TRANFORMER_MAPPING.put("miscNotes", new StudentDataFieldAdapter("Notes", "textarea"));
		FIELD_TRANFORMER_MAPPING.put("fullySponsored", new StudentDataFieldAdapter("Fully Sponsored", "switch", new FullySponsoredTransformer()));
		FIELD_TRANFORMER_MAPPING.put("favoriteSubject", new StudentDataFieldAdapter("Favorite Subject", "text"));
		FIELD_TRANFORMER_MAPPING.put("etapestryAccountNumber", new StudentDataFieldAdapter("E-Tapestry Acccount Number", "number"));
		FIELD_TRANFORMER_MAPPING.put("sponsorYear", new StudentDataFieldAdapter("Sponsor Year", "number"));
		FIELD_TRANFORMER_MAPPING.put("schoolLevel", new StudentDataFieldAdapter("School Level", "select", new SchoolLevelIdTransformer(null))); //pass null as DAO since we won't be doing any parsing
		FIELD_TRANFORMER_MAPPING.put("community", new StudentDataFieldAdapter("Community", "select", new CommunityIdTransformer(null))); //pass null as DAO since we won't be doing any parsing
		FIELD_TRANFORMER_MAPPING.put("region", new StudentDataFieldAdapter("Show in Region", "select", new RegionTransformer()));
	}

    @HiddenFrontEnd
	@DatabaseField(generatedId=true)
	private int id; 
	
	@DatabaseField(columnName="first_name")
	private String firstName;
	
	@DatabaseField(columnName="last_name")
	private String lastName;
	
	@DatabaseField(columnName="known_as")
	private String knownAs;
	
	@DatabaseField(columnName="gender")
	private String gender;
	
	@DatabaseField(columnName="birthdate")
	private Date birthDate;

    @ReadOnlyFrontEnd
	@DatabaseField(columnName="date_created")
	private Date dateCreated;
	
	@DatabaseField(columnName="image_path")
	private String imagePath;
	
	@DatabaseField(columnName="grade")
	private int grade; 
	
	@DatabaseField(columnName="biography")
	private String biography; 
	
	@DatabaseField(columnName="future_aspirations")
	private String futureAspirations; 
	
	@DatabaseField(columnName="misc_notes")
	private String miscNotes; 
	
	@DatabaseField(columnName="fully_sponsored")
	private String fullySponsored; 
	
	@DatabaseField(columnName="favorite_subject")
	private String favoriteSubject;
	private Period age;
	
	@DatabaseField(columnName="etapestry_account_number")
	private int etapestryAccountNumber;
	
	@DatabaseField(columnName="sponsor_year")
	private int sponsorYear;

	@DatabaseField(columnName = "region_id")
	private String region;
	
	
	/**
	 * Foreign Key sections
	 */
	@DatabaseField(columnName="school_level_id", foreign=true)
	private SchoolLevelData schoolLevel; 
	
	@DatabaseField(columnName="community_id", foreign=true)
	private CommunityData community;
	
	@ForeignCollectionField()
	private ForeignCollection<StudentScholarshipLinkData> allAssociatedScholarships;
	
	private List<StudentScholarshipLinkData> availableScholarships;
	

	public  StudentData (){
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @return the knownAs
	 */
	public String getKnownAs() {
		return knownAs;
	}

	/**
	 * @param knownAs the knownAs to set
	 */
	public void setKnownAs(String knownAs) {
		this.knownAs = knownAs;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}
	
	public String getFormattedBirthDate(){
		return new SimpleDateFormat("MM/dd/yyyy").format(birthDate);
	}
	
	/**
	 * lazily instantiates the age var
	 * @return
	 */
	public Period getAge(){
		if(age == null){
			age = new Period(birthDate != null ? new LocalDate(birthDate) : new LocalDate(), new LocalDate(), PeriodType.yearMonthDay());
		}
		return age;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	
	
	/**
	 * @return the dateCreated
	 */
	public Date getDateCreated() {
		return dateCreated;
	}

	/**
	 * @param dateCreated the dateCreated to set
	 */
	public void setDateCreated(Date dateCreated) {
		this.dateCreated = dateCreated;
	}

	/**
	 * @return the imagePath
	 */
	public String getImagePath() {
		return imagePath;
	}

	/**
	 * @param imagePath the imagePath to set
	 */
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the grade
	 */
	public int getGrade() {
		return grade;
	}

	/**
	 * @param grade the grade to set
	 */
	public void setGrade(int grade) {
		this.grade = grade;
	}

	/**
	 * @return the biography
	 */
	public String getBiography() {
		return biography;
	}

	/**
	 * @param biography the biography to set
	 */
	public void setBiography(String biography) {
		this.biography = biography;
	}

	/**
	 * @return the futureAspirations
	 */
	public String getFutureAspirations() {
		return futureAspirations;
	}

	/**
	 * @param futureAspirations the futureAspirations to set
	 */
	public void setFutureAspirations(String futureAspirations) {
		this.futureAspirations = futureAspirations;
	}

	/**
	 * @return the miscNotes
	 */
	public String getMiscNotes() {
		return miscNotes;
	}

	/**
	 * @param miscNotes the miscNotes to set
	 */
	public void setMiscNotes(String miscNotes) {
		this.miscNotes = miscNotes;
	}

	/**
	 * @return the fullySponsored
	 */
	public boolean isFullySponsored() {
		return "y".equals(fullySponsored);
	}

	/**
	 * @param fullySponsored the fullySponsored to set
	 */
	public void setFullySponsored(String fullySponsored) {
		this.fullySponsored = fullySponsored;
	}
	
	public String getFullySponsored(){
		return fullySponsored;
	}
	


	/**
	 * @return the schoolLevel
	 */
	public SchoolLevelData getSchoolLevel() {
		return schoolLevel;
	}

	/**
	 * @param schoolLevel the schoolLevel to set
	 */
	public void setSchoolLevel(SchoolLevelData schoolLevel) {
		this.schoolLevel = schoolLevel;
	}

	/**
	 * @return the community
	 */
	public CommunityData getCommunity() {
		return community;
	}

	/**
	 * @param community the community to set
	 */
	public void setCommunity(CommunityData community) {
		this.community = community;
	}
	
	
	
	/**
	 * @return the allAssociatedScholarships
	 */
	public ForeignCollection<StudentScholarshipLinkData> getAllAssociatedScholarships() {
		return allAssociatedScholarships;
	}

	/**
	 * @param allAssociatedScholarships the allAssociatedScholarships to set
	 */
	public void setAllAssociatedScholarships(
			ForeignCollection<StudentScholarshipLinkData> allAssociatedScholarships) {
		this.allAssociatedScholarships = allAssociatedScholarships;
		
	}

	/**
	 * This list is lazily instantiated
	 * @return the availableScholarships
	 */
	public List<StudentScholarshipLinkData> getAvailableScholarships() {
        if(availableScholarships == null){
            availableScholarships = new ArrayList<StudentScholarshipLinkData>();
        }

		if(CollectionUtils.isEmpty(availableScholarships) && CollectionUtils.isNotEmpty(allAssociatedScholarships)){
			this.availableScholarships = new ArrayList<StudentScholarshipLinkData>();
			for(StudentScholarshipLinkData data : allAssociatedScholarships){
				if(data.isAvaliable()){
					availableScholarships.add(data);
				}
			}

			//sort scholarships
			Collections.sort(availableScholarships, new Comparator<StudentScholarshipLinkData>(){
				//by default we sort by scholarship amounts DESC (so most expensive one is listed first)
				public int compare(StudentScholarshipLinkData o1,
						StudentScholarshipLinkData o2) {
					return o2.getScholarship().getAmount() - o1.getScholarship().getAmount();
				}

			});
		}

		return availableScholarships;
	}
	

	/**
	 * @return the etapestryAccountNumber
	 */
	public int getEtapestryAccountNumber() {
		return etapestryAccountNumber;
	}

	/**
	 * @param etapestryAccountNumber the etapestryAccountNumber to set
	 */
	public void setEtapestryAccountNumber(int etapestryAccountNumber) {
		this.etapestryAccountNumber = etapestryAccountNumber;
	}

	/**
	 * @return the sponsorYear
	 */
	public int getSponsorYear() {
		return sponsorYear;
	}

	/**
	 * @param sponsorYear the sponsorYear to set
	 */
	public void setSponsorYear(int sponsorYear) {
		this.sponsorYear = sponsorYear;
	}

	/**
	 * @return the favoriteSubject
	 */
	public String getFavoriteSubject() {
		return favoriteSubject;
	}

	/**
	 * @param favoriteSubject the favoriteSubject to set
	 */
	public void setFavoriteSubject(String favoriteSubject) {
		this.favoriteSubject = favoriteSubject;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	/**
	 * Retrieve the eligibility of this Student 
	 * in terms of filtering on the landing page
	 * 
	 * There are currently 4 filters
	 * 	1. Gender
	 *  2. Grade
	 *  3. Community
	 *  4. Scholarship type
	 * @return
	 */
	public JsonObject getFilterEligibility(){
		JsonObject eligibility= new JsonObject();
		
		//first, let's create our more complicated eligibility options
		
		//grade is treated as an array because the can search 
		//on either single grade or school level 
		JsonArray gradeEligibility = new JsonArray();
		gradeEligibility.add(new JsonPrimitive(schoolLevel.getName()));
		gradeEligibility.add(new JsonPrimitive(grade + "")); //add the grade as a string because the search value will be a string
		
		//scholarships is simply a list of all scholarship type available
		//it doesn't matter if there are duplicates in the list
		JsonArray scholarshipTypeEligibility = new JsonArray();
		for(StudentScholarshipLinkData data : availableScholarships){
			scholarshipTypeEligibility.add(new JsonPrimitive(data.getScholarship().getName()));
		}
		
		//set all of our eligibilities back to source JsonObject 
		//note that these keys need to match the name attributes 
		//the search fields for everything to tie together properly
		eligibility.add("genderCriteria", new JsonPrimitive(gender));
		eligibility.add("gradeCriteria", gradeEligibility);
		eligibility.add("communityCriteria", new JsonPrimitive(community.getName()));
		eligibility.add("scholarshipCriteria", scholarshipTypeEligibility);
		
		
		return eligibility; 
	}


    /**
     * Dumps all of the fields of the Student in a Json format
     * @return
     */
	public JsonObject getFieldInfo(){
		JsonObject fieldsInfo = new JsonObject();
		
		JsonArray fields = new JsonArray();
		
		//use reflection to iterate through fields
		for(Field field : StudentData.class.getDeclaredFields()){
			//get all fields with annotations
			if(field.getAnnotation(DatabaseField.class) != null){
				JsonObject curField = new JsonObject();
				
				curField.add("name", new JsonPrimitive(field.getName()));
				StudentDataFieldAdapter adapter = FIELD_TRANFORMER_MAPPING.get(field.getName());
				
				curField.add("inputType", new JsonPrimitive(adapter != null ? adapter.getInputType() : "text"));
				JsonElement value = new JsonPrimitive("");
                JsonElement displayValue =  new JsonPrimitive("");
				
					try {
						if(adapter != null){
							String strVal = null;
                            String strDisplayValue = null;
							if(adapter.getTransformer() != null){
                                if (field.get(this) != null) {
                                    strDisplayValue = adapter.getTransformer().transformToDisplay(field.get(this));
                                    strVal = adapter.getTransformer().transformToData(field.get(this));
                                }
							}else{
                                Object objVal = field.get(this);
                                strVal = objVal != null ? objVal.toString() : null;
								strDisplayValue = strVal;
							}
							
							value = strVal != null ? new JsonPrimitive(strVal) : value;
                            displayValue = strDisplayValue != null ? new JsonPrimitive(strDisplayValue) : displayValue;
						}
					} catch (IllegalArgumentException e) {
						//just skip this field
					} catch (IllegalAccessException e) {
						//just skip this field
					}
				curField.add("value", value);
				curField.add("displayValue", displayValue);
				curField.add("label", new JsonPrimitive(adapter != null ? adapter.getDisplayLabel() : field.getName()));
                curField.add("readOnly", new JsonPrimitive(field.getAnnotation(ReadOnlyFrontEnd.class) != null));
                curField.add("hidden", new JsonPrimitive(field.getAnnotation(HiddenFrontEnd.class) != null));
				fields.add(curField);
			}
		}
		
		fieldsInfo.add("fields", fields);
		return fieldsInfo;
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	

}
