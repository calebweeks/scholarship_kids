/**
 * 
 */
package org.coeduc.dao;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.ForeignCollectionField;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author calebweeks
 *
 */

@DatabaseTable(tableName="scholarship_types")
public class ScholarshipTypeData implements Comparable<ScholarshipTypeData> {
	
	@DatabaseField(generatedId=true)
	private int id; 
	
	@DatabaseField(columnName="type_name")
	private String name; 
	
	@DatabaseField
	private String description;
	
	@DatabaseField(columnName="etapestry_value")
	private String etapestryValue;

    @DatabaseField(columnName = "type_code", unique = true)
    private String typeCode;

	@ForeignCollectionField(eager = true)
	private ForeignCollection<RegionScholarshipLinkData> regionalizedData;
	private Map<String, RegionScholarshipLinkData> mappedRegionData;

	public ScholarshipTypeData(){
		
	}

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    /**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	public String getDisplayName(String region){
		if(region == null){
			region = "us";
		}

		if(getMappedRegionData().containsKey(region) && StringUtils.isNotEmpty(getMappedRegionData().get(region).getAlternateSpelling())){
			 return getMappedRegionData().get(region).getAlternateSpelling();
		}else{
			return getName();
		}


	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	public ForeignCollection<RegionScholarshipLinkData> getRegionalizedData() {
		return regionalizedData;
	}

	public void setRegionalizedData(ForeignCollection<RegionScholarshipLinkData> regionalizedData) {
		this.regionalizedData = regionalizedData;


	}

	public Map<String, RegionScholarshipLinkData> getMappedRegionData(){
		if(mappedRegionData == null){
			mappedRegionData = new HashMap<>();
			for(RegionScholarshipLinkData data : regionalizedData){
				mappedRegionData.put(data.getRegion().getId(), data);
			}
		}
		return mappedRegionData;
	}

	/**
	 *
	 * @return amount for default region
	 */
	public int getAmount(){
		return getAmount("us");
	}

	public int getAmount(String region){
		if(region == null){
			region = "us";
		}

		if(getMappedRegionData().containsKey(region)){
			return getMappedRegionData().get(region).getAmount();
		}else{
			throw new IllegalArgumentException("Invalid region supplied: " + region);
		}
	}



	/**
	 *
	 * @return currency for default region
	 */
	public String getCurrency(){
		return getCurrency("us");
	}

	public String getCurrency(String region){
		if(region == null){
			region = "us";
		}

		if(getMappedRegionData().containsKey(region)){
			return getMappedRegionData().get(region).getRegion().getCurrency();
		}else{
			throw new IllegalArgumentException("Invalid region supplied: " + region);
		}
	}

	/**
	 * @return the etapestryKey
	 */
	public String getEtapestryValue() {
		return etapestryValue;
	}

	/**
	 * @param etapestryValue the etapestryKey to set
	 */
	public void setEtapestryValue(String etapestryValue) {
		this.etapestryValue = etapestryValue;
	}

	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
	@Override 
	public int hashCode(){
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object o){
		return o instanceof ScholarshipTypeData && 
			   ((ScholarshipTypeData)o).id == id;
	}

	/* (non-Javadoc)
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	public int compareTo(ScholarshipTypeData o) {
		return getAmount() - o.getAmount();
	}

}
