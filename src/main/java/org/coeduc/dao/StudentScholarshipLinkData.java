/**
 * 
 */
package org.coeduc.dao;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.coeduc.jsonAdapters.JsonExclusion;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * @author calebweeks
 *
 *
 *This class represents the many to many relationship linking table for 
 *students and scholarships
 */
@DatabaseTable(tableName="kid_scholarship_link")
public class StudentScholarshipLinkData {
	
	@DatabaseField(generatedId=true)
	private int id; 
	
	@JsonExclusion
	@DatabaseField(columnName="scholarship_kid_id", foreign=true)
	private StudentData student; 
	@DatabaseField(columnName="scholarship_type_id", foreign=true, foreignAutoRefresh=true)
	private ScholarshipTypeData scholarship; 
	
	@DatabaseField(columnName="scholarship_status")
	private String status; 
	
	@DatabaseField(columnName="scholarship_sponsor_name")
	private String sponsorName;
	
	public StudentScholarshipLinkData(){
		
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the student
	 */
	public StudentData getStudent() {
		return student;
	}

	/**
	 * @param student the student to set
	 */
	public void setStudent(StudentData student) {
		this.student = student;
	}

	/**
	 * @return the scholarship
	 */
	public ScholarshipTypeData getScholarship() {
		return scholarship;
	}

	/**
	 * @param scholarship the scholarship to set
	 */
	public void setScholarship(ScholarshipTypeData scholarship) {
		this.scholarship = scholarship;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the sponsorName
	 */
	public String getSponsorName() {
		return sponsorName;
	}

	/**
	 * @param sponsorName the sponsorName to set
	 */
	public void setSponsorName(String sponsorName) {
		this.sponsorName = sponsorName;
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
	}
	
	public boolean isAvaliable(){
		return "available".equalsIgnoreCase(status);
	}

}
