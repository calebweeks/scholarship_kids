package org.coeduc.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.coeduc.data.adapters.HiddenFrontEnd;

/**
 * Created by caleb on 3/26/16.
 */
@DatabaseTable(tableName = "regions")
public class RegionData {

	@HiddenFrontEnd
	@DatabaseField(id = true)
	private String id;

	@DatabaseField
	private String name;

	@DatabaseField
	private String currency;

	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getCurrency(){
		return currency;
	}
}
