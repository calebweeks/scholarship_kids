/**
 * 
 */
package org.coeduc.dao;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcPooledConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import org.coeduc.App;
import org.coeduc.config.AppConfiguration.DbConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;

/**
 * @author calebweeks
 *
 */
public class DBHelper {
	private static Logger log = LoggerFactory.getLogger(DBHelper.class);
	
	private static JdbcPooledConnectionSource connectionPool;
			
	public static void initialize() throws SQLException{
		DbConfiguration dbConfig = App.CONFIGURATION.getDbConfig();
		
		StringBuilder connectionString = new StringBuilder();
		connectionString.append("jdbc:mysql://")
		.append(dbConfig.getHost()).append(":")
		.append(dbConfig.getPort()).append("/")
		.append(dbConfig.getDatabase()).append("?user=")
		.append(dbConfig.getUsername()).append("&password=")
		.append(dbConfig.getPassword());
		
		log.info("DB connection string: " + connectionString.toString());
		connectionPool = new JdbcPooledConnectionSource(connectionString.toString());
		connectionPool.setMaxConnectionAgeMillis(5 * 60 * 1000);
		connectionPool.setCheckConnectionsEveryMillis(60 * 1000);
		connectionPool.setTestBeforeGet(true);
		
	}
	/**
	 * Retrieve a Connection to the Db configured for the App
	 * @return
	 * @throws SQLException 
	 */
	public static ConnectionSource getDBConnection() throws SQLException{
		
		if(connectionPool != null){
			return connectionPool;
		}else{
			throw new SQLException("The Connction pool has not yet been initialized. Must call DBHelper.initialize before ConnectionSources can be provided");
		}
		
	}


	/**
	 * Get an instance of a StudentDao
	 * @return
	 * @throws SQLException
	 */
	public static  Dao<StudentData, Integer> getStudentDao() throws SQLException{
		Dao<StudentData, Integer> dao = DaoManager.createDao(getDBConnection(), StudentData.class);
		dao.setObjectCache(true);
		return dao;
	}
	public static  Dao<ScholarshipTypeData, Integer> getScholarshipTypeDao() throws SQLException{
		Dao<ScholarshipTypeData, Integer> dao = DaoManager.createDao(getDBConnection(), ScholarshipTypeData.class);
		dao.setObjectCache(true);
		return dao;
	}
	public static  Dao<CommunityData, Integer> getCommunityDao() throws SQLException{
		Dao<CommunityData, Integer> dao = DaoManager.createDao(getDBConnection(), CommunityData.class);
		dao.setObjectCache(true);
		return dao;
	}
	public static  Dao<SchoolLevelData, Integer> getSchoolLevelDao() throws SQLException{
		Dao<SchoolLevelData, Integer> dao = DaoManager.createDao(getDBConnection(), SchoolLevelData.class);
		dao.setObjectCache(true);
		return dao;
	}
	public static  Dao<StudentScholarshipLinkData, Integer> getStudentScholarshipLinkDao() throws SQLException{
		Dao<StudentScholarshipLinkData, Integer> dao =  DaoManager.createDao(getDBConnection(), StudentScholarshipLinkData.class);
		dao.setObjectCache(true);
		return dao;
	}
    public static  Dao<AdminCredentialData, Integer> getAdminCredentialDao() throws SQLException{
		Dao<AdminCredentialData, Integer> dao = DaoManager.createDao(getDBConnection(), AdminCredentialData.class);
		dao.setObjectCache(true);
		return dao;
    }
    public static Dao<AdditionalDonationMappingData, Integer> getAdditionalDonationDao() throws SQLException{
		Dao<AdditionalDonationMappingData, Integer> dao = DaoManager.createDao(getDBConnection(), AdditionalDonationMappingData.class);
		dao.setObjectCache(true);
		return dao;
    }
    public static Dao<DonationFrequencyData, Integer> getDonationFrequencyDao() throws SQLException{
		Dao<DonationFrequencyData, Integer> dao = DaoManager.createDao(getDBConnection(), DonationFrequencyData.class);
		dao.setObjectCache(true);
		return dao;
    }
	public static Dao<RegionData, Integer> getRegionDao() throws SQLException{
		Dao<RegionData, Integer> dao = DaoManager.createDao(getDBConnection(), RegionData.class);
		dao.setObjectCache(true);
        return dao;
    }

}
