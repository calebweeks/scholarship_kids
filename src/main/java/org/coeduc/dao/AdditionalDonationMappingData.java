package org.coeduc.dao;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.reflect.TypeToken;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.apache.commons.lang3.StringUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by caleb on 10/27/14.
 */
@DatabaseTable(tableName = "additional_donation_mapping")
public class AdditionalDonationMappingData {

    @DatabaseField(generatedId=true)
    private int id;

    @DatabaseField(columnName="scholarship_type_code" )
    private String scholarshipTypeCode;

    @DatabaseField(columnName="donation_frequency_code")
    private String donationFrequencyCode;

    @DatabaseField(columnName = "default_amount")
    private int defaultAmount;

    @DatabaseField
    private String amounts;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getScholarshipTypeCode() {
        return scholarshipTypeCode;
    }

    public void setScholarshipTypeCode(String scholarshipTypeCode) {
        this.scholarshipTypeCode = scholarshipTypeCode;
    }

    public String getDonationFrequencyCode() {
        return donationFrequencyCode;
    }

    public void setDonationFrequencyCode(String donationFrequencyCode) {
        this.donationFrequencyCode = donationFrequencyCode;
    }

    public int getDefaultAmount() {
        return defaultAmount;
    }

    public void setDefaultAmount(int defaultAmount) {
        this.defaultAmount = defaultAmount;
    }

    public String getAmounts() {
        return amounts;
    }

    public void setAmounts(String amounts) {
        this.amounts = amounts;
    }

    public List<Integer> getAmountsList() {
        //set the amounts list also
        if(StringUtils.isNotBlank(amounts)){
            Type typeToken = new TypeToken<List<Integer>>(){}.getType();
            return new Gson().fromJson(amounts, typeToken);
        }else{
            return new ArrayList<>();
        }
    }

    public void setAmountsList(List<Integer> amountsList) {
        //need to also set the amounts JSON array
        amounts = new Gson().toJson(amountsList);
    }
}
