package org.coeduc.dao;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * Created by caleb on 10/27/14.
 */
@DatabaseTable(tableName = "donation_frequency")
public class DonationFrequencyData {

    @DatabaseField(generatedId=true)
    private int id;

    @DatabaseField
    private String label;

    @DatabaseField
    private String description;

    @DatabaseField(columnName = "payment_factor")
    private Integer paymentFactor;

    @DatabaseField(columnName = "display_template")
    private String displayTemplate;

    @DatabaseField(columnName = "etapestry_value")
    private String etapestryValue;

    @DatabaseField(columnName = "frequency_code", unique = true)
    private String frequencyCode;

    public String getFrequencyCode() {
        return frequencyCode;
    }

    public void setFrequencyCode(String frequencyCode) {
        this.frequencyCode = frequencyCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDisplayTemplate() {
        return displayTemplate;
    }

    public void setDisplayTemplate(String displayTemplate) {
        this.displayTemplate = displayTemplate;
    }

    public String getEtapestryValue() {
        return etapestryValue;
    }

    public void setEtapestryValue(String etapestryValue) {
        this.etapestryValue = etapestryValue;
    }

    public Integer getPaymentFactor() {

        return paymentFactor;
    }

    public void setPaymentFactor(Integer paymentFactor) {
        this.paymentFactor = paymentFactor;
    }

    public String toString(){
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
}
