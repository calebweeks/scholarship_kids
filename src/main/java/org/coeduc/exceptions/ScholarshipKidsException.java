/**
 * 
 */
package org.coeduc.exceptions;

/**
 * @author calebweeks
 *
 */
public abstract class ScholarshipKidsException extends Exception {
	private static final long serialVersionUID = 1L;
	
	public ScholarshipKidsException(String msg){
		super(msg);
	}
	
	public ScholarshipKidsException(Exception e){
		super(e);
	}
	
	public ScholarshipKidsException(String msg, Exception e){
		super(msg, e);
	}

}
