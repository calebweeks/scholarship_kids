/**
 * 
 */
package org.coeduc.exceptions;

/**
 * @author calebweeks
 *
 */
public class ApplicationFailureException extends ScholarshipKidsException {
	/**
	 * @param msg
	 * @param e
	 */
	public ApplicationFailureException(String msg, Exception e) {
		super(msg, e);
	}

	private static final long serialVersionUID = 1L;

}
