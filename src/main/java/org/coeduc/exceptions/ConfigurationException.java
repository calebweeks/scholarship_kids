/**
 * 
 */
package org.coeduc.exceptions;

/**
 * @author calebweeks
 *
 */
public class ConfigurationException extends ScholarshipKidsException{
	private static final long serialVersionUID = 1L;

	public ConfigurationException(Exception e){
		super(e);
	}
	
	public ConfigurationException(String msg, Exception e){
		super(msg, e);
	}
	
	public ConfigurationException(){
		super("Not all neccessary configuration could be found. Please check the System Properties or the indicated config file for accuracy");
	}
}
