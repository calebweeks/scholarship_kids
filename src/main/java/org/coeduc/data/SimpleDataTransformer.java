/**
 * 
 */
package org.coeduc.data;

/**
 * @author calebweeks
 *
 */
public abstract class SimpleDataTransformer implements DataTransformer<String> {

	/* (non-Javadoc)
	 * @see org.coeduc.data.DataTransformer#parse(java.lang.String)
	 */
	public String parse(String input) {
		return input;
	}

    @Override
    public String transformToData(Object input) {
        return input.toString();
    }
}
