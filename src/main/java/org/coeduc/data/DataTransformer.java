/**
 * 
 */
package org.coeduc.data;

/**
 * @author calebweeks
 *
 */
public interface DataTransformer<T> {
	
	public String transformToDisplay(Object input);
	public String transformToData(Object input);
	public T parse(String input);

}
