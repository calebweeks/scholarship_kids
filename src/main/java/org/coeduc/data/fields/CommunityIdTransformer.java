/**
 * 
 */
package org.coeduc.data.fields;

import org.coeduc.dao.CommunityData;
import org.coeduc.data.ForeignDataTransformer;

import com.j256.ormlite.dao.Dao;

/**
 * @author calebweeks
 *
 */
public class CommunityIdTransformer extends ForeignDataTransformer<CommunityData>{

	
	public CommunityIdTransformer(Dao<CommunityData, Integer> dao){
		super(dao);
	}
	/* (non-Javadoc)
	 * @see org.coeduc.data.ForeignDataTransformer#transform(java.lang.Object)
	 */
	public String transformToDisplay(Object input) {
		return ((CommunityData)input).getName();
	}

    @Override
    public String transformToData(Object input) {
        return ((CommunityData)input).getId() + "";
    }

	

}
