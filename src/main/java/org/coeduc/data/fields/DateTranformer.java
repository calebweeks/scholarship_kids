/**
 * 
 */
package org.coeduc.data.fields;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.coeduc.data.DataTransformer;

/**
 * @author calebweeks
 *
 */
public class DateTranformer implements DataTransformer<Date> {

	private static SimpleDateFormat htmlDateInputFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat displayDateFormat = new SimpleDateFormat("MM/dd/yyyy");
	/* (non-Javadoc)
	 * @see org.coeduc.data.DataTransformer#trasform(java.lang.Object)
	 */
	public String transformToDisplay(Object input) {
		// TODO Auto-generated method stub
		return displayDateFormat.format(input);
	}

    @Override
    public String transformToData(Object input) {
        return htmlDateInputFormat.format(input);
    }

    /* (non-Javadoc)
         * @see org.coeduc.data.DataTransformer#parse(java.lang.String)
         */
	public Date parse(String input) {
		// TODO Auto-generated method stub
		try {
			return htmlDateInputFormat.parse(input);
		} catch (ParseException e) {
			return null; 
		}
	}

}
