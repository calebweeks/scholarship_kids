/**
 * 
 */
package org.coeduc.data.fields;

import org.coeduc.dao.SchoolLevelData;
import org.coeduc.data.ForeignDataTransformer;

import com.j256.ormlite.dao.Dao;

/**
 * @author calebweeks
 *
 */
public class SchoolLevelIdTransformer extends ForeignDataTransformer<SchoolLevelData>{
	
	public SchoolLevelIdTransformer(Dao<SchoolLevelData, Integer> dao){
		super(dao);
	}

	/* (non-Javadoc)
	 * @see org.coeduc.data.ForeignDataTransformer#transform(java.lang.Object)
	 */
	public String transformToDisplay(Object input) {
		return ((SchoolLevelData)input).getName();
	}

    @Override
    public String transformToData(Object input) {
        return ((SchoolLevelData)input).getId() + "";
    }
}
