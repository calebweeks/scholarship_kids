/**
 * 
 */
package org.coeduc.data.fields;

import org.coeduc.data.SimpleDataTransformer;

/**
 * @author calebweeks
 *
 */
public class GenderTransformer extends SimpleDataTransformer{

	/* (non-Javadoc)
	 * @see org.coeduc.data.DataTransformer#trasform(java.lang.Object)
	 */
	public String transformToDisplay(Object input) {
		// TODO Auto-generated method stub
		return "m".equalsIgnoreCase((String)input) ? "Male" : "Female";
	}

}
