/**
 * 
 */
package org.coeduc.data.fields;

import org.coeduc.data.SimpleDataTransformer;

/**
 * @author calebweeks
 *
 */
public class FullySponsoredTransformer extends SimpleDataTransformer{

	/* (non-Javadoc)
	 * @see org.coeduc.data.DataTransformer#transform(java.lang.Object)
	 */
	public String transformToDisplay(Object input) {
		
		// TODO Auto-generated method stub
		return "y".equalsIgnoreCase((String)input) ? "Yes" : "No";
	}

}
