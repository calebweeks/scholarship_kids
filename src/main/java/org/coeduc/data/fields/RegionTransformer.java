package org.coeduc.data.fields;

import org.coeduc.data.SimpleDataTransformer;

/**
 * Created by caleb on 3/26/16.
 */
public class RegionTransformer extends SimpleDataTransformer {

	/* (non-Javadoc)
	 * @see org.coeduc.data.DataTransformer#trasform(java.lang.Object)
	 */
	public String transformToDisplay(Object input) {
		String displayValue = "Unknown";

		if("us".equals((String)input)){
			displayValue = "United States";
		}else if("ca".equals((String)input)){
			displayValue = "Canada";
		}else if("--".equals((String)input)){
			displayValue = "All";
		}
		return displayValue;
	}
}
