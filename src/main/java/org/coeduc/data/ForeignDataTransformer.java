/**
 * 
 */
package org.coeduc.data;

import com.j256.ormlite.dao.Dao;

import java.sql.SQLException;

/**
 * @author calebweeks
 *
 */
public abstract class ForeignDataTransformer<T> implements DataTransformer<T>{
	
	private Dao<T, Integer> dao; 
	
	public ForeignDataTransformer(Dao<T, Integer> dao){
		this.dao = dao;
	}
	
	public T parse(String input) {
		try {
			return dao.queryForId(Integer.parseInt(input));
		} catch (NumberFormatException e) {
			throw new IllegalArgumentException("Failed to parse ID from input", e);
		} catch (SQLException e) {
			throw new RuntimeException("Issue with Dao", e);
		}
	}

}
