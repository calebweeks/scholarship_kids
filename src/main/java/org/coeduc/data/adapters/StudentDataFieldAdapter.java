/**
 * 
 */
package org.coeduc.data.adapters;

import org.coeduc.data.DataTransformer;

/**
 * @author calebweeks
 *
 */
public class StudentDataFieldAdapter {
	private String displayLabel; 
	private String inputType;
	private DataTransformer<?> transformer;
	
	public StudentDataFieldAdapter(String displayLabel, String inputType){
        this(displayLabel, inputType,null);
    }
	public StudentDataFieldAdapter(String displayLabel, String inputType, DataTransformer<?> transformer){
		this.displayLabel = displayLabel;
		this.inputType = inputType;
		this.transformer = transformer;
	}

	/**
	 * @return the displayLabel
	 */
	public String getDisplayLabel() {
		return displayLabel;
	}

	/**
	 * @return the transformer
	 */
	public DataTransformer<?> getTransformer() {
		return transformer;
	}
	
	public String getInputType(){
		return inputType;
	}

	
}
