package org.coeduc.data.adapters.options;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by calebweeks on 2/8/14.
 */
public class SimpleOptionProvider implements OptionsProvider {

    private List<AbstractMap.SimpleEntry<String, String>> options;

    public SimpleOptionProvider(List<AbstractMap.SimpleEntry<String, String>> options)  {
        this.options = options;
    }

    public SimpleOptionProvider(String [] options)  {
        this.options = new ArrayList<AbstractMap.SimpleEntry<String, String>>(options.length);
        for(String val : options){
            this.options.add(new AbstractMap.SimpleEntry<String, String>(val, val));
        }
    }

    @Override
    public List<AbstractMap.SimpleEntry<String, String>> getOptions() {
        return options;
    }
}
