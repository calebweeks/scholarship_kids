package org.coeduc.data.adapters.options;

import java.util.AbstractMap;
import java.util.List;

/**
 * Created by calebweeks on 2/8/14.
 */
public interface OptionsProvider {

    public List<AbstractMap.SimpleEntry<String, String>> getOptions();
}
