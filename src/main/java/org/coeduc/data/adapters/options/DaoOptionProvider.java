package org.coeduc.data.adapters.options;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by calebweeks on 2/8/14.
 */
public abstract class DaoOptionProvider<T> implements OptionsProvider{

    private List<AbstractMap.SimpleEntry<String, String>> options;


    public DaoOptionProvider(List<T> daoOptions){

        options = new ArrayList<AbstractMap.SimpleEntry<String, String>>();

        //iterate through the daoOptions and transform each of them
        for(T obj : daoOptions){
            options.add(convertObjectToOption(obj));
        }
    }

    /**
     * Method that must handle the transformation of the Dao object to
     * an simple key / value pair option
     * @param obj
     * @return
     */
    public abstract AbstractMap.SimpleEntry<String, String> convertObjectToOption(T obj);

    @Override
    public List<AbstractMap.SimpleEntry<String, String>> getOptions() {
        return options;
    }
}
