package org.coeduc;

import static spark.Spark.after;
import static spark.Spark.get;
import static spark.Spark.head;

import java.io.IOException;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.*;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.coeduc.dao.*;
import org.coeduc.spark.decorators.RequestDecorator;
import org.coeduc.spark.routes.Coed404Route;
import org.coeduc.spark.routes.MainTemplateRoute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.Request;
import spark.Response;
import spark.Route;

import com.j256.ormlite.dao.Dao;

/**
 * Hello world!
 */
public class ScholarshipKidsController {

	//logging vars
	Logger log = LoggerFactory.getLogger(ScholarshipKidsController.class);

	//manager for included content from coeduc site
	private StaticIncludeManager includeManager;

	private static final String[] REGIONS = {"us", "ca"};


	public ScholarshipKidsController() throws IOException {

		includeManager = new StaticIncludeManager();

		//redirect
		get("/", (request, response) -> {
			response.redirect("/us");
			return null;
		});

		//redirect for detail page
		get("/detail/:id", (request, response) -> {
			response.redirect("/en/detail/" + request.params("id"));
			return null;
		});

		for(String region : REGIONS) {
			//landing page
			head("/" + region, generateLandingPageRoute("HEAD"));
			get("/" + region, generateLandingPageRoute("GET"));

			//student detail page
			head("/"+region+"/detail/:id", generateDetailPageRoute("HEAD"));
			get("/"+region+"/detail/:id", generateDetailPageRoute("GET"));
		}






		get("/error", new MainTemplateRoute("text/html", includeManager) {

			@Override
			public Object handle(RequestDecorator request,
								 Response response, VelocityContext templateParams) {

				//add all of our params
				templateParams.put("page_title", "Cooperate for Education");
				templateParams.put("actual_template", Constants.FRONT_END_TEMPLATES_ROOT + "error_page.vm");
				StringWriter writer = new StringWriter();
				try {
					getTemplate(Constants.SCHOLARSHIP_KIDS_TEMPLATE).merge(templateParams, writer);
				} catch (Exception e) {
					log.error("Error build page template", e);
//						halt(500, ErrorUtility.getStackTraceAsString(e));
				}
				response.status(200);
				return writer.toString();
			}

		});

		/**
		 * The 404 Route captures all requests that haven't already been
		 * covered by other routes.
		 */
		after(new Coed404Route());


	}


	/**
	 * This route needs to behave slightly differently depending on the type of HTTP request being used.
	 *
	 * @param httpMethod
	 * @return
	 */
	private Route generateLandingPageRoute(final String httpMethod) {
		Route landingPageRoute = new MainTemplateRoute("text/html", includeManager) {

			@Override
			public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
				StringWriter writer = new StringWriter();
				String region = getRegion(request);
				templateParams.put("page_title", "Search for Scholarship Students");
				templateParams.put("actual_template", Constants.FRONT_END_TEMPLATES_ROOT + "landing_page.vm");
				templateParams.put("styleSheets", Arrays.asList("chosen.css", "simplePagination.css", "landing_page.css"));
				templateParams.put("jsFiles", Arrays.asList("chosen.jquery.min.js", "jquery.simplePagination.js", "landing_page.js"));
				try {
					//pull in eligible students from the DB.
					Dao<StudentData, Integer> studentsDao = DBHelper.getStudentDao();
					List<StudentData> students = studentsDao.queryBuilder().where().eq("fully_sponsored", "n").and().in("region_id", Arrays.asList(new String[]{region, "--"})).query();

					//find create a unique set of communities / scholarship types
					//this is done here so that we don't need to make another call to the DB
					Set<CommunityData> communities = new HashSet<CommunityData>();
					Set<ScholarshipTypeData> scholarshipTypes = new HashSet<ScholarshipTypeData>();
					for (StudentData student : students) {
						DBHelper.getCommunityDao().refresh(student.getCommunity());
						DBHelper.getSchoolLevelDao().refresh(student.getSchoolLevel());
						communities.add(student.getCommunity());

						if (CollectionUtils.isNotEmpty(student.getAvailableScholarships())) {
							//iterate through all scholarship types
							for (StudentScholarshipLinkData data : student.getAvailableScholarships()) {
								scholarshipTypes.add(data.getScholarship());
							}
						}
					}
					//sort the students based on scholarships available
					Collections.sort(students, new Comparator<StudentData>() {

						public int compare(StudentData s1, StudentData s2) {
							if(s1.getSponsorYear() != s2.getSponsorYear()){
								return s1.getSponsorYear() - s2.getSponsorYear();
							}else {
								if (CollectionUtils.isEmpty(s1.getAvailableScholarships())) {
									return 1;
								} else if (CollectionUtils.isEmpty(s2.getAvailableScholarships())) {
									return -1;
								} else {
									if (s1.getAvailableScholarships().size() < s2.getAvailableScholarships().size()) {
										return -1;
									} else if (s1.getAvailableScholarships().size() > s2.getAvailableScholarships().size()) {
										return 1;
									} else {
										if (s1.getAvailableScholarships().get(0).getScholarship().getAmount() < s1.getAvailableScholarships().get(0).getScholarship().getAmount()) {
											return -1;
										} else if (s1.getAvailableScholarships().get(0).getScholarship().getAmount() > s1.getAvailableScholarships().get(0).getScholarship().getAmount()) {
											return 1;
										} else {
											return s1.getDateCreated().compareTo(s1.getDateCreated());
										}
									}
								}
							}
						}

					});


					//create an ordered list from the Set - order by community name
					List<CommunityData> orderedCommunities = new ArrayList<CommunityData>(communities);
					Collections.sort(orderedCommunities);

					//create ordered list of Scholarship type - ordered by amount DESC (largest amount listed first
					List<ScholarshipTypeData> orderedScholarshipTypes = new ArrayList<ScholarshipTypeData>(scholarshipTypes);
					Collections.sort(orderedScholarshipTypes, new Comparator<ScholarshipTypeData>() {

						public int compare(ScholarshipTypeData o1,
										   ScholarshipTypeData o2) {
							return o2.getAmount() - o1.getAmount();
						}

					});


					//add all params needed to template
					templateParams.put("students", students);
					templateParams.put("communities", orderedCommunities);
					templateParams.put("scholarshipTypes", orderedScholarshipTypes);
					getTemplate(Constants.SCHOLARSHIP_KIDS_TEMPLATE).merge(templateParams, writer);
				} catch (SQLException e) {
					log.error("Error retrieving scholarship students", e);
					if (StringUtils.equals("HEAD", httpMethod)) {
						response.status(500);
					} else {
						response.redirect("/error");
					}
				} catch (Exception e) {
					log.error("Error renderining scholarship landing page", e);
					if (StringUtils.equals("HEAD", httpMethod)) {
						response.status(500);
					} else {
						response.redirect("/error");
					}
				}
				response.status(200);
				return StringUtils.equals("HEAD", httpMethod) ? "" : writer.toString();
			}

		};

		return landingPageRoute;
	}


	/**
	 * This route needs to behave slightly differently depending on the type of HTTP request being used.
	 *
	 * @param httpMethod
	 * @return
	 */
	private Route generateDetailPageRoute(final String httpMethod) {
		Route detailRoute = new MainTemplateRoute("text/html", includeManager) {

			@Override
			public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
				StringWriter writer = new StringWriter();
				//student to show detail for
				StudentData student = null;
				List<DonationFrequencyData> frequencyOptions = null;
				List<AdditionalDonationMappingData> additionalDonationMapping = null;

				//pull in id from request
				String id = request.params(":id");

				//make sure we've got an int
				try {
					int intId = Integer.parseInt(id);

					//pull in student from dao
					Dao<StudentData, Integer> studentsDao = DBHelper.getStudentDao();
					student = studentsDao.queryForId(intId);

					//pull in the donation frequency options
					frequencyOptions = DBHelper.getDonationFrequencyDao().queryForAll();

					//retrieve the additional mappings
					additionalDonationMapping = DBHelper.getAdditionalDonationDao().queryForAll();

				} catch (NumberFormatException e) {
					log.error("Invalid student id requested: " + id);
					student = null; //no student
				} catch (SQLException e) {
					log.error("Error retrieving student with ID = " + id + " from DB:", e);
					if (StringUtils.equals("HEAD", httpMethod)) {
						response.status(500);
					} else {
						response.redirect("/error");
					}
				}

				//make sure we have a student
				if (student != null) {

					//add all of our params
					templateParams.put("page_title", student.getKnownAs());
					templateParams.put("actual_template", Constants.FRONT_END_TEMPLATES_ROOT + "detail_page.vm");
					templateParams.put("student", student);
					templateParams.put("styleSheets", Arrays.asList("chosen.css", "detail_page.css"));
					templateParams.put("jsFiles", Arrays.asList("chosen.jquery.min.js", "detail_page.js"));


					//add all of the frequency options
					templateParams.put("frequencyOptions", frequencyOptions);


					//convert the additional donation mapping to a JSON Object and store it for client side use
					templateParams.put("additionalDonationMapping", convertMappingToJsonString(additionalDonationMapping));

					//add clientside data
					templateParams.put("clientData", generateClientSideData(student));


					try {
						getTemplate(Constants.SCHOLARSHIP_KIDS_TEMPLATE).merge(templateParams, writer);
					} catch (Exception e) {
						log.error("Error build page template", e);
						if (StringUtils.equals("HEAD", httpMethod)) {
							response.status(500);
						} else {
							response.redirect("/error");
						}
					}
					response.status(200);
					return StringUtils.equals("HEAD", httpMethod) ? "" : writer.toString();
				} else {
					//if we didn't find a student, redirect to landing page
					response.redirect(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/");
					return null;
				}
			}

		};

		return detailRoute;
	}


	/**
	 * Takes the List of mappings and creates a tiered Key, Value pair mapping
	 *
	 * @param mappingList
	 * @return
	 */
	private String convertMappingToJsonString(List<AdditionalDonationMappingData> mappingList) {
		//a Java version of our Map
		Map<String, Map<String, Map<String, Object>>> adaptedMap = new HashMap<>();

		//iterate through mappings and create map where necessary
		for (AdditionalDonationMappingData curMap : mappingList) {
			if (!adaptedMap.containsKey(curMap.getScholarshipTypeCode())) {
				adaptedMap.put(curMap.getScholarshipTypeCode(), new HashMap<>());
			}

			//get the correct map and set the amounts for the frequency
			Map<String, Object> donationOption = new HashMap<>();
			donationOption.put("defaultVal", curMap.getDefaultAmount());
			donationOption.put("values", curMap.getAmountsList());

			adaptedMap.get(curMap.getScholarshipTypeCode()).put(curMap.getDonationFrequencyCode(), donationOption);
		}

		//now convert the map to a JSON string
		return new Gson().toJson(adaptedMap);
	}

	/**
	 * Creates a JsonObject that is made available for client side use
	 * containing a very curated set of info
	 */
	private String generateClientSideData(StudentData student) {
		JsonObject object = new JsonObject();
		JsonObject studentObj = new JsonObject();

		//populate student object
		studentObj.add("knownAs", new JsonPrimitive(student.getKnownAs()));
		//Yes, there are only 12 years, but subtract from 13 so the math works out correctly
		// ex) 12 - 7 = 5, but there are 6 years of school for a 7th grader, assuming you include 7th
		studentObj.add("yearsRemaining", new JsonPrimitive(13 - student.getGrade()));

		//add objects to client side item
		object.add("student", studentObj);

		return object.toString();
	}

}
