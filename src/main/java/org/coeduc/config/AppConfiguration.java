/**
 * 
 */
package org.coeduc.config;

import com.google.gson.JsonObject;

/**
 * @author calebweeks
 *
 */
public class AppConfiguration {
	private String environment; 
	private ServerConfiguration serverConfig; 
	private DbConfiguration dbConfig; 
	
	public AppConfiguration(JsonObject object){
		this.environment = object.get("environment").getAsString();
		this.serverConfig = new ServerConfiguration(object.get("server").getAsJsonObject());
		this.dbConfig = new DbConfiguration(object.get("db").getAsJsonObject());
	}
	
	
	
	
	/**
	 * @return the environment
	 */
	public String getEnvironment() {
		return environment;
	}


	/**
	 * @return the serverConfig
	 */
	public ServerConfiguration getServerConfig() {
		return serverConfig;
	}


	/**
	 * @return the dbConfig
	 */
	public DbConfiguration getDbConfig() {
		return dbConfig;
	}




	/***************************************
	 * Configuration Classes
	***************************************/
	
	public class ServerConfiguration{
		private int port; 
		private String applicationPath; 
		
		public ServerConfiguration(JsonObject object){
			//need to determine if the port should be set
			//if the port specified == NONE, then we won't set it ourselves
			port = "NONE".equals(object.get("port").getAsString()) ? -1 : object.get("port").getAsInt();
			applicationPath = object.get("applicationPath").getAsString();
		}

		/**
		 * @return the port
		 */
		public int getPort() {
			return port;
		}

		/**
		 * @return the applicationPath
		 */
		public String getApplicationPath() {
			return applicationPath;
		}
		
		
	}
	
	
	public class DbConfiguration{
		
		private int port; 
		private String host; 
		private String username; 
		private String password; 
		private String database; 
		
		public DbConfiguration(JsonObject object){
			port = object.get("port").getAsInt();
			host = object.get("host").getAsString();
			username = object.get("username").getAsString();
			password = object.get("password").getAsString();
			database = object.get("database").getAsString();
		}

		/**
		 * @return the port
		 */
		public int getPort() {
			return port;
		}

		/**
		 * @return the host
		 */
		public String getHost() {
			return host;
		}

		/**
		 * @return the username
		 */
		public String getUsername() {
			return username;
		}

		/**
		 * @return the password
		 */
		public String getPassword() {
			return password;
		}

		/**
		 * @return the database
		 */
		public String getDatabase() {
			return database;
		}
		
		
	}

}


