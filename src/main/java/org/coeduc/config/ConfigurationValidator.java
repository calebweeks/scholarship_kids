/**
 * 
 */
package org.coeduc.config;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;

import org.apache.commons.lang3.StringUtils;
import org.coeduc.exceptions.ConfigurationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonElement;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSyntaxException;
import com.google.gson.stream.JsonReader;

/**
 * @author calebweeks
 *
 */
public class ConfigurationValidator {
	
	private static final String SYSTEM_PROP_ROOT = "org_coeduc_scholarshipkids_config";
	private static final String ENVIRONMENT_CONFIG = SYSTEM_PROP_ROOT + "_environment";
	private static final String SERVER_PORT_CONFIG = SYSTEM_PROP_ROOT + "_server_port";
	private static final String SERVER_APPLICATION_PATH_CONFIG = SYSTEM_PROP_ROOT + "_server_applicationPath";
	private static final String DB_PORT_CONFIG = SYSTEM_PROP_ROOT + "_db_port";
	private static final String DB_HOST_CONFIG = SYSTEM_PROP_ROOT + "_db_host";
	private static final String DB_DATABASE_CONFIG = SYSTEM_PROP_ROOT + "db_database";
	private static final String DB_USERNAME_CONFIG = SYSTEM_PROP_ROOT + "_db_username";
	private static final String DB_PASSWORD_CONFIG = SYSTEM_PROP_ROOT + "_db_password";
	private static final String DB_FULL_URL_CONFIG = SYSTEM_PROP_ROOT + "_db_fullUrl";

	private static final Logger log = LoggerFactory.getLogger(ConfigurationValidator.class);
	/**
	 * Parses the file at filePath. This is expected to be a JsonObject
	 * @param filePath
	 * @return
	 * @throws JsonIOException
	 * @throws JsonSyntaxException
	 * @throws FileNotFoundException
	 */
	public static JsonObject setupConfigFromFile(String filePath) throws JsonIOException, JsonSyntaxException, FileNotFoundException{
		
		return new JsonParser().parse(new JsonReader(new FileReader(filePath))).getAsJsonObject(); 
	}
	
	/**
	 * checks for a number of System properties that should be used by the app
	 * and creates a JsonObject from them
	 * @return
	 * @throws ConfigurationException 
	 */
	public static JsonObject setupConfigFromSystemProperties() throws ConfigurationException{
		JsonObject config = new JsonObject();
		JsonObject serverConfig = new JsonObject();
		JsonObject dbConfig = new JsonObject();
		//server config
		serverConfig.add("port", getSystemEnv(SERVER_PORT_CONFIG));
		serverConfig.add("applicationPath", getSystemEnv(SERVER_APPLICATION_PATH_CONFIG));
		
		//db config - either pull in piecemeal or use the provided full URL
		String fullUrl = getSystemEnvAsString(DB_FULL_URL_CONFIG);
		
		if(StringUtils.isEmpty(fullUrl)){
			dbConfig.add("port", getSystemEnv(DB_PORT_CONFIG));
			dbConfig.add("host", getSystemEnv(DB_HOST_CONFIG));
			dbConfig.add("database", getSystemEnv(DB_DATABASE_CONFIG));
			dbConfig.add("username", getSystemEnv(DB_USERNAME_CONFIG));
			dbConfig.add("password", getSystemEnv(DB_PASSWORD_CONFIG));
		}else{
			try {
				URI dbUri = new URI(fullUrl);
				
				int dbPort = dbUri.getPort();
				log.info("DB Port: " + dbPort);
				if(dbPort < 0){
					log.info("No DB Port assigned. Defaulting to 3306");
					dbPort = 3306; 
				}
				dbConfig.add("port", new JsonPrimitive( dbPort));
				
				log.info("DB Host: " + dbUri.getHost());
				dbConfig.add("host", new JsonPrimitive( dbUri.getHost()));
				
				String dbDatabase = dbUri.getPath();
				log.info("DB Database: " + dbDatabase);
				
				if(StringUtils.isEmpty(dbDatabase) || dbDatabase.length() == 1 && dbDatabase.charAt(0) == '/'){
					log.info("DB Database is empty. Defaulting to 'coeduc_scholarshipkids'");
					dbDatabase = "coeduc_scholarshipkids";
				}
				if(dbDatabase.startsWith("/")){
					dbDatabase = dbDatabase.substring(1);
				}
				
				dbConfig.add("database", new JsonPrimitive( dbDatabase));
				
				String [] userInfo = dbUri.getUserInfo().split(":");
				
				log.info("User Info: " + Arrays.toString(userInfo));
				
				dbConfig.add("username", new JsonPrimitive( userInfo[0]));
				dbConfig.add("password", new JsonPrimitive( userInfo.length > 1 ? userInfo[1] : ""));
			} catch (URISyntaxException e) {
				throw new ConfigurationException("Invalid DB URL", e);
			}
		}
		
		config.add("server", serverConfig);
		config.add("db", dbConfig);
		config.add("environment", getSystemEnv(ENVIRONMENT_CONFIG));
		return config; 
	}
	
	private static JsonElement getSystemEnv(String propName){
		return new JsonPrimitive(getSystemEnvAsString(propName));
	}
	
	private static String getSystemEnvAsString(String propName){
		String sysVal = System.getenv(propName);
		
		if(StringUtils.isNotEmpty(sysVal) && sysVal.contains("SYSTEM_ENV('")){
			String newSystemVar = sysVal.replace("SYSTEM_ENV('", "").replace("')", "");
			
			if(StringUtils.isNotEmpty(newSystemVar)){
				sysVal = getSystemEnvAsString(newSystemVar);
			}
			
		}
		
		//don't allow null to be returned
		sysVal = (sysVal == null ? "" : sysVal);
		
		log.info("Value for System property " + propName + " = "  + sysVal);
		
		return sysVal == null ? "" : sysVal;
	}
	
	/**
	 * checks the static app configuration JsonObject to ensure 
	 * everything is there. 
	 * @return
	 */
	public static boolean isValidConfiguration(JsonObject conf){
		return conf != null &&
			   hasStringMember(conf, "environment") &&
			   hasValidServerConfiguration(conf) && 
			   hasValidDbConfiguration(conf); 
	}
	
	/****************************************************
	 The following methods are simply utility methods for 
	 verifying specific pieces of the application config
	 *****************************************************/
	private static boolean hasValidDbConfiguration(JsonObject conf){
		return hasObjectMember(conf, "db") &&
			   hasStringMember(getObjectMember(conf, "db"), "port") &&
			   hasStringMember(getObjectMember(conf, "db"), "host") &&
			   hasStringMember(getObjectMember(conf, "db"), "username") &&
			   hasStringMember(getObjectMember(conf, "db"), "password") &&
			   hasStringMember(getObjectMember(conf, "db"), "database");
	}
	
	private static boolean hasValidServerConfiguration(JsonObject conf){
		return hasObjectMember(conf, "server") &&
			   hasStringMember(getObjectMember(conf, "server"), "port") &&
			   hasStringMember(getObjectMember(conf, "server"), "applicationPath");
	}
	
	
	
	/**
	 *  A number of helper functions for determining JsonObject content
	 */
	private static boolean hasObjectMember(JsonObject object, String memberName){
		return object.has(memberName) && object.get(memberName).isJsonObject();
	}
	
	private static JsonObject getObjectMember(JsonObject object, String memberName){
		return object.get(memberName).getAsJsonObject(); 
	}
	
	private static boolean hasStringMember(JsonObject object, String memberName){
		return object.has(memberName) && object.get(memberName).isJsonPrimitive();
	}
	
}
