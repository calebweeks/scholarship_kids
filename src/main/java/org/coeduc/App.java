/**
 * 
 */
package org.coeduc;


import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonSyntaxException;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.LogManager;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.runtime.RuntimeConstants;
import org.coeduc.config.AppConfiguration;
import org.coeduc.config.ConfigurationValidator;
import org.coeduc.dao.DBHelper;
import org.coeduc.exceptions.ApplicationFailureException;
import org.coeduc.exceptions.ConfigurationException;
import org.coeduc.logging.RequestLogHelper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Spark;
import spark.servlet.SparkApplication;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;

/**
 * @author calebweeks
 * 
 */
public class App implements SparkApplication{
	
	/* (non-Javadoc) this is needed for execution inside of 
	 * an application server. 
	 * @see spark.servlet.SparkApplication#init()
	 */
	//we are being loaded in the context of a web server. 
	//given that, then we assume the config file is located in 
	//~/.scholarshipkids/scholarshipkids.conff
	public void init() {
		try {
			//pull in path to user home
			String home = System.getProperty("user.home");
			
			//simply invoke main providing our assumed config path
			App.main(new String[]{"--config-from=" +home + "/.scholarshipKids/scholarshipKids.config"});
		} catch (ApplicationFailureException e) {
			e.printStackTrace();
		}
		
	}
	
	
	//configuration will be stored in a JSON Object
	public static AppConfiguration CONFIGURATION;

	//path of the directory the JAR or source file is executed from
	//this is really only useful if you're running the app outside of a
	//web server like tomcat
	private static final String EXECUTION_PATH = App.class.getProtectionDomain().getCodeSource().getLocation().getPath().endsWith("/") ?
												 App.class.getProtectionDomain().getCodeSource().getLocation().getPath() : 
												 App.class.getProtectionDomain().getCodeSource().getLocation().getPath().substring(0, App.class.getProtectionDomain().getCodeSource().getLocation().getPath().lastIndexOf("/") + 1);
	//by default we will look in the same directory as the app is executed
	private static final String DEFAULT_CONFIG_FILE_PATH = EXECUTION_PATH + "scholarshipKids.config" ; 
	
	private static Logger log = LoggerFactory.getLogger(App.class);
	
	
	/**
	 * We have 3 options for starting the app
	 * 1) simply execute the jar - this will look for a config file in the same dir as the JAR called scholarshipKids.config. An error is thrown if not found.
	 * 2) add the arg "--config-from=system" - this will attempt to load all necessary config from System Properties
	 * 3) add the arg "--config-from=<path>" - this will look for a config file at the specified path
	 * @param args
	 * @throws ApplicationFailureException 
	 * @throws IOException
	 */
	public static void main(String[] args) throws ApplicationFailureException{
		try{
			
			log.error(System.getenv("test_env_var"));
			//first let's pull in the configuration for the app
			retrieveConfiguration(args);
			
			//check to make sure we have all the configuration we need
			//otherwise throw an error
			log.info("Successfully pulled in configuration");
			
			initializeLoggingConfiguration();
			
			initializeApplicationServer();
			
			DBHelper.initialize();
			
			RequestLogHelper.initializeLogPreRequestFilter();
			
			initializeTemplateEngine();
			
			initializeApplicationControllers();
			
			RequestLogHelper.initializeLogPostRequestFilter();
			
			log.info("Application startup has completed!");
		
			
		}catch(Exception e){
			log.error("Scholarship Kids has suffered a catastrophic failure.", e);
			throw new ApplicationFailureException("Scholarship Kids has suffered a catastrophic failure.", e);
		} 
		

		
	}

	/**
	 * initialize Template Engine...in this case Velocity
	 */
	private static void initializeTemplateEngine(){
		Properties prop = new Properties();
		prop.setProperty(RuntimeConstants.INPUT_ENCODING, "UTF-8");
		prop.setProperty(RuntimeConstants.OUTPUT_ENCODING, "UTF-8");
		prop.setProperty("runtime.log.logsystem.class", "org.coeduc.logging.VelocitySlf4jLogChute");


        if(StringUtils.equals(App.CONFIGURATION.getEnvironment(), "DEVELOPMENT")){
            prop.setProperty("resource.loader", "file");
            prop.setProperty("file.resource.loader.path", Paths.get("").resolve("src/main/resources").toAbsolutePath().normalize().toString());
            prop.setProperty("file.resource.loader.cache", "false");
            prop.setProperty("velocimacro.library.autoreload", "true");
        }else{
            prop.setProperty("resource.loader", "class");
            prop.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            prop.setProperty("class.resource.loader.cache", "true");
        }
		Velocity.init(prop);
	}
	/**
	 * Create the controllers that actually do all the lifting
	 * @throws IOException 
	 */
	private static void initializeApplicationControllers() throws IOException{
		//kick off all of the app controllers
		//new ApiController();
		new ScholarshipKidsController();
		new AdminController(); 
	}
	
	/**
	 * Sets the port for the server from the configuration. 
	 */
	private static void initializeApplicationServer(){
		//we only want to initialize the server config
		//if we're running as a standalone app (ie, not as
		//a web app in a servlet container)
		if(App.CONFIGURATION.getServerConfig().getPort() > 0){
			//setup the public directory
			Spark.staticFileLocation("/public");
			Spark.setPort(App.CONFIGURATION.getServerConfig().getPort());
		}
	}
	
	/**
	 * sets up some initial app configuration
	 */
	private static void initializeLoggingConfiguration(){
		
		//we don't want to include debug statements on live
		if(CONFIGURATION.getEnvironment().equals("PRODUCTION")){
			log.info("Setting log level to INFO");
			LogManager.getRootLogger().setLevel(Level.INFO);
		}else if(CONFIGURATION.getEnvironment().equals("STAGING")){
			log.info("Setting log level to DEBUG");
			LogManager.getRootLogger().setLevel(Level.DEBUG);
		}else if(CONFIGURATION.getEnvironment().equals("DEVELOPMENT")){
			log.info("Setting log level to DEBUG");
			LogManager.getRootLogger().setLevel(Level.DEBUG);
		}
	}
	
	/**
	 * Pulls in configuration from files or system parameters
	 * @param args
	 * @throws FileNotFoundException 
	 * @throws JsonSyntaxException 
	 * @throws JsonIOException 
	 * @throws ConfigurationException 
	 */
	private static void retrieveConfiguration(String [] args) throws JsonIOException, JsonSyntaxException, FileNotFoundException, ConfigurationException{
		//we need to determine how we're pulling in configuration
		//either from the system or from a config file
		boolean useSystemProperties = false; 
		String configFilePath = DEFAULT_CONFIG_FILE_PATH;
		
		for(String arg : args){
			if(arg.startsWith("--config-from=")){
				//remove the --config-from bit and any quotes
				String configFrom = arg.replace("--config-from=", "").replaceAll("\"", "");
				
				if("system".equalsIgnoreCase(configFrom)){
					useSystemProperties = true;
				}else{
					//assume this is a file location - either absolute or relative to dir housing JAR
					if(configFrom.startsWith("/")){
						configFilePath = configFrom;
					}else{
						configFilePath = EXECUTION_PATH + configFrom;
					}
				}
			}
		}
		
		JsonObject rawConfig = null; 
		
		if(useSystemProperties){
			log.info("Attempting to pull configuration from System Properties");
			rawConfig = ConfigurationValidator.setupConfigFromSystemProperties();
		}else{
			log.info("Attempting to pull configuration from the file: " + configFilePath);
			rawConfig = ConfigurationValidator.setupConfigFromFile(configFilePath);
		}
		
		if(ConfigurationValidator.isValidConfiguration(rawConfig)){
			CONFIGURATION = new AppConfiguration(rawConfig);
		}else{
			//throw an exception since we don't have all the configuration we need
			throw new ConfigurationException();
		}
	}

	
	
	

}
