package org.coeduc.model;

import org.coeduc.dao.StudentData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by caleb on 10/26/14.
 *
 * This Class represents a List of Students grouped
 * by the student's sponsorship year
 */
public class StudentGrouping {


    private List<StudentData> sponsoredStudents;
    private List<StudentData> nonFullySponsoredStudents;


    public StudentGrouping() {
        sponsoredStudents = new ArrayList<>();
        nonFullySponsoredStudents = new ArrayList<>();
    }

    public void addSponsoredStudent(StudentData student){
        sponsoredStudents.add(student);
    }

    public void addNonFullySponsoredStudent(StudentData student){
        nonFullySponsoredStudents.add(student);
    }

    public List<StudentData> getSponsoredStudents() {
        return sponsoredStudents;
    }

    public void setSponsoredStudents(List<StudentData> sponsoredStudents) {
        this.sponsoredStudents = sponsoredStudents;
    }

    public List<StudentData> getNonFullySponsoredStudents() {
        return nonFullySponsoredStudents;
    }

    public void setNonFullySponsoredStudents(List<StudentData> nonFullySponsoredStudents) {
        this.nonFullySponsoredStudents = nonFullySponsoredStudents;
    }
}
