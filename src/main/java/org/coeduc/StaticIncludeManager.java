/**
 * 
 */
package org.coeduc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author calebweeks
 *This class manages the static HTML that is pulled in from the shared resources on the 
 * the coeduc site. Not an incredibly efficient way of doing this, but the alternative approach
 * would be doing it via AJAX on the client.
 */
public class StaticIncludeManager {
	
	private static Logger log = LoggerFactory.getLogger(StaticIncludeManager.class);
	
	//24 hours
	private static Long EXPIRATION_TIME = new Long(1000 * 60 * 60 * 24); 
			
	private static String HEADER_CONTENT_URL = "http://www.coeduc.org/resources/includes/header.html"; 
	private static String NAVIGATION_CONTENT_URL = "http://www.coeduc.org/resources/includes/navigation.html"; 
	private static String FOOTER_CONTENT_URL = "http://www.coeduc.org/resources/includes/footer.html"; 
	
	//this maps the resource to the last time it was updated. 
	//every hour this should be updated
	private Map<String, Long> resourceTimeManagement;
	
	//this Map keeps track of which resources are currently being updated
	//A true value indicated the resource is being updated, a null value
	//means it is not. 
	private Map<String, Boolean> resourceUpdatingManagement;
	
	
	/**
	 * Content that is pulled in statically
	 */
	
	private String headerContent;  
	private String navigationContent;
	private String footerContent; 
	
	
	public StaticIncludeManager(){
		resourceTimeManagement = new HashMap<String, Long>();
		resourceUpdatingManagement = new HashMap<String, Boolean>();
		
		//read in the initial content
		headerContent = loadStaticContentFromExternalUrl(HEADER_CONTENT_URL);
		navigationContent = loadStaticContentFromExternalUrl(NAVIGATION_CONTENT_URL);
		footerContent = loadStaticContentFromExternalUrl(FOOTER_CONTENT_URL);
	}
	
	/**
	 * 
	 * @return - if the expiration time has passed or hasn't happened yet
	 */
	private boolean contentIsExpired(String resourceManagementKey){
		return resourceTimeManagement.get(resourceManagementKey) == null || 
			   (System.currentTimeMillis() - resourceTimeManagement.get(resourceManagementKey) > EXPIRATION_TIME); 
	}
	
	/**
	 * 
	 * This method opens a URL connection to the indicated URL and pulls in the content
	 * as a String value. It is a BLOCKING task!
	 * @throws IOException 
	 * 
	 */
	private String loadStaticContentFromExternalUrl(String urlString){
		log.info("Loading static content from the following URL: " + urlString);
		//create the URL Object and a Reader to parse the content
		URL url;
		StringBuilder builder = new StringBuilder(); 
		try {
			url = new URL(urlString);
			
			BufferedReader reader = new BufferedReader(new InputStreamReader(url.openStream()));
			
			
			//read the response, line by line and add it to our String. 
			String curLine = reader.readLine();
			while(curLine != null){
				builder.append(curLine);
				curLine = reader.readLine();
			}
			
			//update the last time this piece of content was updated
			resourceTimeManagement.put(urlString, System.currentTimeMillis());
		} catch (Exception e) {
			log.error("Error pulling in static content from " + urlString, e);
			//make sure an empty string is returned
			builder = new StringBuilder();
		}
		
		
		
		
		//return the full content
		return builder.toString();
	}
	
	/**
	 * This method runs the execute method of the loader in a new thread. 
	 * All logic concerning locking should be handled in the loader. 
	 * Additionally, this method is not synchronized deliberately - the idea 
	 * is that multiple resources can be loaded at the same time. A single resource
	 * should only be loaded once. The invocation of this method should be wrapped
	 * in some syncronized block
	 */
	private void asyncLoadStaticContent(final AsynchronousStaticResourceLoader loader){
		new Thread(new Runnable(){

			public void run() {
				loader.execute();
			}
			
		}).start();
	}
	
	/**
	 * @return the headerContent
	 */
	public synchronized String getHeaderContent() {
		if(contentIsExpired(HEADER_CONTENT_URL)){
			headerContent = loadStaticContentFromExternalUrl(HEADER_CONTENT_URL);
		}
		return headerContent;
	}
	
	/**
	 * @return the navigationContent
	 */
	public synchronized String getNavigationContent() {
		if(contentIsExpired(NAVIGATION_CONTENT_URL)){
			navigationContent = loadStaticContentFromExternalUrl(NAVIGATION_CONTENT_URL);
		}
		return navigationContent;
	}
	
	/**
	 * @return the footerContent
	 */
	public synchronized String getFooterContent() {
		if(contentIsExpired(FOOTER_CONTENT_URL)){
			footerContent = loadStaticContentFromExternalUrl(FOOTER_CONTENT_URL);
		}
		return footerContent;
	} 
	
	
	/**
	 * This class is a simple wrapper for a Thread 
	 * that 
	 * @author calebweeks
	 *
	 */
	private abstract class AsynchronousStaticResourceLoader implements Runnable{
		
		/**
		 * This method will be implemented by each of the invokers
		 */
		public abstract void execute();

		
	}
	
	
	

}
