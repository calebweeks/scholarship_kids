/**
 * 
 */
package org.coeduc.api;

import java.sql.SQLException;
import java.util.List;

import org.coeduc.dao.DBHelper;
import org.coeduc.dao.StudentScholarshipLinkData;
import org.coeduc.jsonAdapters.ScholarshipLinkExclusionStrategy;
import org.coeduc.spark.decorators.RequestDecorator;

import spark.Response;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.j256.ormlite.dao.Dao;

/**
 * @author calebweeks
 *
 */
public class ScholarshipsApi extends AbstractApi {

	/**
	 */
	public ScholarshipsApi() {
		super("/api/scholarships");
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#getAll(org.coeduc.spark.decorators.RequestDecorator, spark.Response)
	 */
	@Override
	public Object getAll(RequestDecorator request, Response response) {
		try {
			Dao<StudentScholarshipLinkData, Integer> studentDao = DBHelper.getStudentScholarshipLinkDao();
			
			//get all students
			List<StudentScholarshipLinkData> students = studentDao.queryForAll();
			
			//TODO: filter all students based on query params
			Gson gson = new GsonBuilder().
					setExclusionStrategies(new ScholarshipLinkExclusionStrategy()).create();
			return gson.toJson(students);
		} catch (SQLException e) {
			return renderException(response, e);
		}
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#create(org.coeduc.spark.decorators.RequestDecorator, spark.Response)
	 */
	@Override
	public Object create(RequestDecorator request, Response response) {
		// TODO Auto-generated method stub
		try {
			Gson gson = new GsonBuilder().setExclusionStrategies(new ScholarshipLinkExclusionStrategy()).create();
			StudentScholarshipLinkData scholarship = gson.fromJson(request.raw().getReader(), StudentScholarshipLinkData.class);
			DBHelper.getStudentScholarshipLinkDao().create(scholarship);
			response.status(201);
			return gson.toJson(scholarship);
		} catch (Exception e) {
			return renderException(response, e);
		}
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#update(org.coeduc.spark.decorators.RequestDecorator, spark.Response, java.lang.Integer)
	 */
	@Override
	public Object update(RequestDecorator request, Response response, Integer id) {
		try {
			Dao<StudentScholarshipLinkData, Integer> dao = DBHelper.getStudentScholarshipLinkDao();
			
			Gson gson = new GsonBuilder().setExclusionStrategies(new ScholarshipLinkExclusionStrategy()).create();
			StudentScholarshipLinkData updatedScholarship = gson.fromJson(request.raw().getReader(), StudentScholarshipLinkData.class);
			dao.update(updatedScholarship);
			
			//pull in the student data again
			return gson.toJson(dao.queryForId(id));
			
		} catch (Exception e) {
			return renderException(response, e);
		}
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#delete(org.coeduc.spark.decorators.RequestDecorator, spark.Response, java.lang.Integer)
	 */
	@Override
	public Object delete(RequestDecorator request, Response response, Integer id) {
		try {
			Dao<StudentScholarshipLinkData, Integer> dao = DBHelper.getStudentScholarshipLinkDao();
			StudentScholarshipLinkData scholarship = dao.queryForId(id);
			dao.delete(scholarship);
			JsonObject jsonRes = new JsonObject();
			jsonRes.add("status", new JsonPrimitive("success")); 
			return jsonRes;
			
		} catch (SQLException e) {
			return renderException(response, e);
		}
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#getOne(org.coeduc.spark.decorators.RequestDecorator, spark.Response, java.lang.Integer)
	 */
	@Override
	public Object getOne(RequestDecorator request, Response response, Integer id) {
		try {
			Gson gson = new GsonBuilder().setExclusionStrategies(new ScholarshipLinkExclusionStrategy()).create();
			
			//pull in the student data
			return gson.toJson(DBHelper.getStudentScholarshipLinkDao().queryForId(id));
			
		} catch (Exception e) {
			return renderException(response, e);
		}
	}

}
