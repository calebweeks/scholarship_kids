/**
 * 
 */
package org.coeduc.api;

import java.sql.SQLException;
import java.util.List;

import org.coeduc.dao.DBHelper;
import org.coeduc.dao.StudentData;
import org.coeduc.jsonAdapters.ScholarshipLinkExclusionStrategy;
import org.coeduc.spark.decorators.RequestDecorator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.j256.ormlite.dao.Dao;

import spark.Response;

/**
 * @author calebweeks
 *
 */
public class StudentDataApi extends AbstractApi {

	/**
	 * @param collectionPaths
	 */
	public StudentDataApi() {
		super("/api/students");
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#getAll(org.coeduc.spark.decorators.RequestDecorator, spark.Response)
	 */
	@Override
	public Object getAll(RequestDecorator request, Response response) {
		try {
			Dao<StudentData, Integer> studentDao = DBHelper.getStudentDao();
			
			//get all students
			List<StudentData> students = studentDao.queryForAll();
			
			//TODO: filter all students based on query params
			Gson gson = new GsonBuilder().
					setExclusionStrategies(new ScholarshipLinkExclusionStrategy()).create();
			return gson.toJsonTree(students);
		} catch (SQLException e) {
			return renderException(response, e);
		}
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#create(org.coeduc.spark.decorators.RequestDecorator, spark.Response)
	 */
	@Override
	public Object create(RequestDecorator request, Response response) {
		try {
			Gson gson = new GsonBuilder().setExclusionStrategies(new ScholarshipLinkExclusionStrategy()).create();
			StudentData student = gson.fromJson(request.raw().getReader(), StudentData.class);
			DBHelper.getStudentDao().create(student);
			response.status(201);
			return gson.toJson(student);
		} catch (Exception e) {
			return renderException(response, e);
		}
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#update(org.coeduc.spark.decorators.RequestDecorator, spark.Response)
	 */
	@Override
	public Object update(RequestDecorator request, Response response, Integer id) {
		try {
			Dao<StudentData, Integer> dao = DBHelper.getStudentDao();
			
			Gson gson = new GsonBuilder().setExclusionStrategies(new ScholarshipLinkExclusionStrategy()).create();
			StudentData updatedStudent = gson.fromJson(request.raw().getReader(), StudentData.class);
			dao.update(updatedStudent);
			
			//pull in the student data again
			return gson.toJson(dao.queryForId(id));
			
		} catch (Exception e) {
			return renderException(response, e);
		}
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#delete(org.coeduc.spark.decorators.RequestDecorator, spark.Response)
	 */
	@Override
	public Object delete(RequestDecorator request, Response response, Integer id) {
		try {
			Dao<StudentData, Integer> dao = DBHelper.getStudentDao();
			StudentData student = dao.queryForId(id);
			dao.delete(student);
			JsonObject jsonRes = new JsonObject();
			jsonRes.add("status", new JsonPrimitive("success")); 
			return jsonRes;
			
		} catch (SQLException e) {
			return renderException(response, e);
		}
	}

	/* (non-Javadoc)
	 * @see org.coeduc.api.AbstractApi#getOne(org.coeduc.spark.decorators.RequestDecorator, spark.Response)
	 */
	@Override
	public Object getOne(RequestDecorator request, Response response, Integer id) {
		try {
			
			Gson gson = new GsonBuilder().setExclusionStrategies(new ScholarshipLinkExclusionStrategy()).create();
			
			//pull in the student data
			return gson.toJson(DBHelper.getStudentDao().queryForId(id));
			
		} catch (Exception e) {
			return renderException(response, e);
		}
	}

}
