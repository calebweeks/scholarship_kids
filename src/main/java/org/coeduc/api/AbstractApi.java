/**
 * 
 */
package org.coeduc.api;

import org.coeduc.spark.decorators.RequestDecorator;
import org.coeduc.spark.routes.JsonRoute;
import org.coeduc.util.ErrorUtility;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;

import spark.Response;
import spark.Spark;

/**
 * @author calebweeks
 *
 */
public abstract class AbstractApi {
	
	public AbstractApi(String collectionPaths){
		
		Spark.get(collectionPaths, new JsonRoute(){
			@Override
			public Object handleRequest(RequestDecorator request,
					spark.Response response) {
				return getAll(request, response);
			}
			
		});
		Spark.post(collectionPaths, new JsonRoute(){
			@Override
			public Object handleRequest(RequestDecorator request,
					spark.Response response) {
				return create(request, response);
			}
			
		});
		Spark.put(collectionPaths + "/:id", new JsonRoute(){
			@Override
			public Object handleRequest(RequestDecorator request,
					spark.Response response) {
				return updateConcreteImpl(request, response);
			}
			
		});
		Spark.delete(collectionPaths + "/:id", new JsonRoute(){
			@Override
			public Object handleRequest(RequestDecorator request,
					spark.Response response) {
				return deleteConcreteImpl(request, response);
			}
			
		});
		Spark.get(collectionPaths + "/:id", new JsonRoute(){
			@Override
			public Object handleRequest(RequestDecorator request,
					spark.Response response) {
				return getOneConcreteImpl(request, response);
			}
			
		});
		
	}
	
	private Object deleteConcreteImpl(RequestDecorator request, Response response){
		return delete(request, response, Integer.parseInt(request.params(":id")));
	}
	
	private Object updateConcreteImpl(RequestDecorator request, Response response){
		return update(request, response, Integer.parseInt(request.params(":id")));
	}
	
	private Object getOneConcreteImpl(RequestDecorator request, Response response){
		return getOne(request, response, Integer.parseInt(request.params(":id")));
	}
	
	public abstract Object getAll(RequestDecorator request, Response response);
	public abstract Object create(RequestDecorator request, Response response);
	public abstract Object update(RequestDecorator request, Response response, Integer id);
	public abstract Object delete(RequestDecorator request, Response response, Integer id);
	public abstract Object getOne(RequestDecorator request, Response response, Integer id);
	
	protected String renderException(Response response, Exception e){
		response.status(500);
		JsonObject json = new JsonObject();
		json.add("status", new JsonPrimitive("error"));
		json.add("errorMsg", new JsonPrimitive(ErrorUtility.getStackTraceAsString(e)));
		return json.toString();
	}

}
