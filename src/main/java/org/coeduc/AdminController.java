/**
 * 
 */
package org.coeduc;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.j256.ormlite.dao.Dao;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.coeduc.dao.*;
import org.coeduc.data.adapters.options.DaoOptionProvider;
import org.coeduc.data.adapters.options.OptionsProvider;
import org.coeduc.data.adapters.options.SimpleOptionProvider;
import org.coeduc.data.fields.DateTranformer;
import org.coeduc.model.StudentGrouping;
import org.coeduc.spark.decorators.RequestDecorator;
import org.coeduc.spark.routes.AdminTemplateRoute;
import org.coeduc.util.ErrorUtility;
import org.coeduc.util.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.*;

import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.*;

import static spark.Spark.*;

/**
 * @author calebweeks
 *
 */
public class AdminController {
	//logging vars
	Logger log = LoggerFactory.getLogger(AdminController.class);
	

	public AdminController(){

        Spark.before("/admin/*", (request, response) -> {
                // check if authenticated
                String user = request.cookie("coedLoginCookie");
                Boolean validLogin = false;

                try {
                    if (user != null) {
                        Dao<AdminCredentialData, Integer> credDao = DBHelper.getAdminCredentialDao();
                        String salt = hash(request.headers("X-FORWARDED-FOR") != null ? request.headers("X-FORWARDED-FOR").getBytes("UTF-8") : request.raw().getRemoteAddr().getBytes("UTF-8"));
                        user = user.split(salt)[0];

                        List<AdminCredentialData> userList = credDao.queryForEq("username", user);

                        validLogin = (userList.size() > 0);
                    }
                } catch (SQLException e) {
                    log.error("SQL Error", e);
                    halt(401, "Internal server error");
                } catch (NoSuchAlgorithmException e) {
                    log.error("Error finding digest algorithm", e);
                    halt(401, "Internal server error");
                } catch (UnsupportedEncodingException e) {
                    log.error("Error finding encoding", e);
                    halt(401, "Internal server error");
                }



                if (!request.pathInfo().contains("login") && !validLogin) {
                    response.status(401);
                    response.redirect(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/login");
                } else if (request.pathInfo().contains("login") && validLogin) {
                    response.redirect(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin");
                }
            });

		//landing page
		get("/admin", new AdminTemplateRoute() {

            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                response.redirect(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students");
                return null;
            }

        });

        //login page
        get("/admin/login", new AdminTemplateRoute() {

            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                StringWriter writer = new StringWriter();
                templateParams.put("page_title", "Login");

                getTemplate(Constants.ADMIN_LOGIN_TEMPLATE_PATH).merge(templateParams, writer);
                response.status(200);
                return writer;
            }
        });
        post("/admin/login", new AdminTemplateRoute() {

            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                String user = request.queryParams("username");
                String pass = request.queryParams("password");
                Boolean validLogin = false;
                String salt;

                try {
                    Dao<AdminCredentialData, Integer> credDao = DBHelper.getAdminCredentialDao();
                    Map<String, Object> args = new HashMap<String, Object>();
                    args.put("username", user);
                    args.put("password", hash(pass.getBytes("UTF-8")));

                    List<AdminCredentialData> userList = credDao.queryForFieldValues(args);
                    validLogin = (userList.size() > 0);
                    salt = hash(request.headers("X-FORWARDED-FOR") != null ? request.headers("X-FORWARDED-FOR").getBytes("UTF-8") : request.raw().getRemoteAddr().getBytes("UTF-8"));
                } catch (SQLException e) {
                    log.error("SQL Error", e);
                    return renderException(response, templateParams, e);
                } catch (NoSuchAlgorithmException e) {
                    log.error("Error finding digest algorithm", e);
                    return renderException(response, templateParams, e);
                } catch (UnsupportedEncodingException e) {
                    log.error("Error finding encoding", e);
                    return renderException(response, templateParams, e);
                }

                if (validLogin) {
                    response.cookie("coedLoginCookie",user + salt, 86400 );

                    response.status(200);
                    response.redirect(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin");
                } else {
                    response.redirect(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/login");
                }

                return null;
            }
        });
		
		//students page
		get("/admin/students", new AdminTemplateRoute(){

			@Override
			public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
				StringWriter writer = new StringWriter();
				templateParams.put("page_title", "Students");
				try {
					Dao<StudentData, Integer> studentDao = DBHelper.getStudentDao();
					
					//get all students in the system
					List<StudentData> allStudents = studentDao.queryForAll();

                    //create a map of year -> StudentGrouping
                    Map<Integer,StudentGrouping> yearGrouping = new HashMap<>();

                    //separate students by year
                    for(StudentData student : allStudents){
                        //see if a student grouping for this student's year already exists
                        if(!yearGrouping.containsKey(student.getSponsorYear())){
                            //create a new map
                            yearGrouping.put(student.getSponsorYear(), new StudentGrouping());
                        }

                        //determine where to add the student
                        if(student.isFullySponsored()){
                            yearGrouping.get(student.getSponsorYear()).addSponsoredStudent(student);
                        }else{
                            yearGrouping.get(student.getSponsorYear()).addNonFullySponsoredStudent(student);
                        }
                    }

                    //create a new sorted map
                    Map<Integer, StudentGrouping> sortedYearGrouping = new LinkedHashMap<>();

                    //get the keyset and sort it
                    List<Integer> sortedKeys = new ArrayList<>(yearGrouping.keySet());
                    Collections.sort(sortedKeys, Collections.reverseOrder());

                    //add the groups in the correct order
                    for(Integer key : sortedKeys){
                        sortedYearGrouping.put(key, yearGrouping.get(key));
                    }


					templateParams.put("yearGrouping", sortedYearGrouping);
                    getTemplate(Constants.STUDENTS_TEMPLATE_PATH).merge(templateParams, writer);
				}catch (Exception e) {
					log.error("Error rendering scholarship landing page", e);
					return renderException(response, templateParams, e);
				}
				response.status(200);
				return writer.toString();
			}
			
		});
		
        get("/admin/students/create", new AdminTemplateRoute() {

            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                StringWriter writer = new StringWriter();
                StudentData student = new StudentData();
                student.setDateCreated(new Date());

                //add all of our params
                templateParams.put("page_title", "Add a New Student");
                templateParams.put("student", student);
                templateParams.put("createStudent", "true");

                try {

                    templateParams.put("optionsProvider", getEditOrCreateStudentOptionParams());

                    getTemplate(Constants.STUDENT_DETAIL_EDIT_TEMPLATE_PATH).merge(templateParams, writer);
                } catch (Exception e) {
                    log.error("Error build page template", e);
                    return renderException(response, templateParams, e);
                }
                response.status(200);
                return writer.toString();
            }
        });
        post("/admin/students/create", new AdminTemplateRoute() {

            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                //student to show detail for
                StudentData student = new StudentData();

                try {
                    Dao<StudentData, Integer> studentsDao = DBHelper.getStudentDao();

                    //fill in student values
                    populateStudentFromRequest(student, request);

                    studentsDao.create(student);

                    int id = studentsDao.extractId(student);

                    //setup the initial scholarships
                    if(!student.isFullySponsored()){
                        Dao<StudentScholarshipLinkData, Integer> scholarshipLinksDao = DBHelper.getStudentScholarshipLinkDao();
                        Dao<ScholarshipTypeData, Integer>  scholarshipTypeDao = DBHelper.getScholarshipTypeDao();

                        StudentScholarshipLinkData scholarshipLink = new StudentScholarshipLinkData();
                        scholarshipLink.setStatus("available");
                        scholarshipLink.setStudent(student);
                        scholarshipLink.setScholarship(scholarshipTypeDao.queryForEq("etapestry_value", "Diploma Scholarship").get(0));
                        StudentScholarshipLinkData scholarshipLink2 = new StudentScholarshipLinkData();
                        scholarshipLink2.setStatus("available");
                        scholarshipLink2.setStudent(student);
                        scholarshipLink2.setScholarship(scholarshipTypeDao.queryForEq("etapestry_value", "Honor Roll Scholarship").get(0));

                        scholarshipLinksDao.create(scholarshipLink);
                        scholarshipLinksDao.create(scholarshipLink2);
                    }

                    // Return jsonObject with status and error message
                    redirectWithMessage(response, App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students/" + id, "Student Created!");

                    return null;
                } catch (NumberFormatException e) {
                    log.error("Input value is not a number", e);
                    return renderException(response, templateParams, e);
                } catch (SQLException e) {
                    log.error("Error retrieving data from DB:", e);
                    return renderException(response, templateParams, e);
                }
            }
        });



		/******** Student detail page ********/
		get("/admin/students/:id", new AdminTemplateRoute(){

			@Override
			public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
				StringWriter writer = new StringWriter();
				//student to show detail for
				StudentData student = null; 
				
				//pull in id from request
				String id = request.params(":id");
				
				//make sure we've got an int
				try{
					int intId = Integer.parseInt(id);

					//pull in student from dao
					Dao<StudentData, Integer> studentsDao = DBHelper.getStudentDao();
					student = studentsDao.queryForId(intId);
				}catch(NumberFormatException e){
					log.error("Invalid student id requested: " + id);
					student = null; 
				}catch(SQLException e){
					log.error("Error retrieving student with ID = " + id + " from DB:", e );
					return renderException(response, templateParams, e);
				}
				
				//make sure we have a student that 
				if(student != null){
					
					//add all of our params
					templateParams.put("page_title", student.getKnownAs());
					templateParams.put("student", student);
					
					try {
						if(StringUtils.equals("edit", request.queryParams("action"))){



                            templateParams.put("optionsProvider", getEditOrCreateStudentOptionParams());

                            getTemplate(Constants.STUDENT_DETAIL_EDIT_TEMPLATE_PATH).merge(templateParams, writer);
						}else{
                            getTemplate(Constants.STUDENT_DETAIL_TEMPLATE_PATH).merge(templateParams, writer);
						}
					} catch (Exception e) {
						log.error("Error build page template", e);
						return renderException(response, templateParams, e);
					}
					response.status(200);
					return writer.toString();
				}else{
					//if we didn't find a student, redirect to landing page
					response.redirect(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students");
					return null; 
				}
			}
			
		});


        /******** Edit student action ********/
        post("/admin/students/:id", new AdminTemplateRoute() {

            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                //student to show detail for
                StudentData student;
                Dao<StudentData, Integer> studentsDao = null;
                String id = request.params(":id");

                //make sure we've got an int
                try{
                    int intId = Integer.parseInt(id);

                    //pull in student from dao
                    studentsDao = DBHelper.getStudentDao();
                    student = studentsDao.queryForId(intId);
                }catch(NumberFormatException e){
                    log.error("Invalid student id requested: " + id);
                    student = null;
                }catch(SQLException e){
                    log.error("Error retrieving student with ID = " + id + " from DB:", e );
                    return renderException(response, templateParams, e);
                }

                if (student != null) {
                    try {
                        //fill in student values
                        populateStudentFromRequest(student, request);

                        studentsDao.update(student);

                        // Return jsonObject with status and error message
                        response.redirect(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students/" + id);
                        return null;
                    } catch(NumberFormatException e) {
                        log.error("Input value is not a number", e);
                        return renderException(response, templateParams, e);
                    } catch (SQLException e) {
                        log.error("Error retrieving data with ID = " + id + " from DB:", e);
                        return renderException(response, templateParams, e);
                    }
                } else {
                    //if we didn't find a student, redirect to landing page
                    redirectWithMessage(response, App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students", "Student updated!");
                    return null;
                }
            }
        });

        /******** Delete student action ********/
        delete("/admin/students/:id", new AdminTemplateRoute() {
            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                //student to show detail for
                StudentData student;
                Dao<StudentData, Integer> studentsDao = null;
                String id = request.params(":id");

                //make sure we've got an int
                try{
                    int intId = Integer.parseInt(id);

                    //pull in student from dao
                    studentsDao = DBHelper.getStudentDao();
                    student = studentsDao.queryForId(intId);
                }catch(NumberFormatException e){
                    log.error("Invalid student id requested: " + id);
                    student = null;
                }catch(SQLException e){
                    log.error("Error retrieving student with ID = " + id + " from DB:", e );
                    return renderException(response, templateParams, e);
                }

                if (student != null) {
                    try {
                        //fetch the scholarship link dao
                        Dao<StudentScholarshipLinkData, Integer> studentScholarshipLinkDao = DBHelper.getStudentScholarshipLinkDao();

                        //delete all the linked scholarships for the student
                        studentScholarshipLinkDao.delete(student.getAllAssociatedScholarships());

                        //delete the actual student
                        studentsDao.delete(student);

                        // Return jsonObject with status and error message
                        return renderJsonSuccess(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students", "Student Deleted!", response);

                    } catch(NumberFormatException e) {
                        log.error("Input value is not a number", e);
                        return renderJsonException(response, e);
                    } catch (SQLException e) {
                        log.error("Error retrieving data with ID = " + id + " from DB:", e);
                        return renderJsonException(response, e);
                    }
                } else {
                    //if we didn't find a student, redirect to landing page
                    return renderJsonException(response, "No student was found matching this ID");
                }
            }
        });




        /*****************************************************************/
        /* Handle the scholarship / student updates */

        //Create new scholarship / student link
        post("/admin/student_scholarships", new AdminTemplateRoute() {
            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {

                //pull in the necessary params
                String studentId = request.queryParams("studentId");
                String scholarshipTypeId = request.queryParams("scholarshipTypeId");

                try {
                    //retrieve our daos
                    Dao<StudentData, Integer> studentDataDao = DBHelper.getStudentDao();
                    Dao<ScholarshipTypeData, Integer> scholarshipTypesDao = DBHelper.getScholarshipTypeDao();
                    Dao<StudentScholarshipLinkData, Integer> scholarshipsLinkDao = DBHelper.getStudentScholarshipLinkDao();

                    //make sure these ids associate with a student and a scholarship
                    StudentData student = studentDataDao.queryForId(Integer.parseInt(studentId));
                    ScholarshipTypeData scholarship = scholarshipTypesDao.queryForId(Integer.parseInt(scholarshipTypeId));


                    //create our new entry
                    StudentScholarshipLinkData newScholarship = new StudentScholarshipLinkData();
                    newScholarship.setStatus("available");
                    newScholarship.setScholarship(scholarship);
                    newScholarship.setStudent(student);

                    //save the entry
                    scholarshipsLinkDao.createOrUpdate(newScholarship);


                } catch (Exception e) {
                    return renderException(response, templateParams, e);
                }


                //if everything went well redirect to the student detail view
                redirectWithMessage(response,App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students/" + studentId, "New Scholarship Added!");
                return null;
            }
        });


        //Delete an existing  scholarship / student link
        delete("/admin/student_scholarships/:id", new AdminTemplateRoute() {
            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                //grab the id provided
                String scholarshipLinkId = request.params(":id");
                String studentId = null;

                try {
                    //pull in the daos
                    Dao<StudentScholarshipLinkData, Integer> studentScholarshipLinkDao = DBHelper.getStudentScholarshipLinkDao();

                    //retrieve the indicated record
                    StudentScholarshipLinkData scholarshipLink = studentScholarshipLinkDao.queryForId(Integer.parseInt(scholarshipLinkId));

                    //get the student and id
                    studentId = scholarshipLink.getStudent().getId() + "";

                    //delete this record
                    studentScholarshipLinkDao.delete(scholarshipLink);

                } catch (Exception e) {
                    return renderJsonException(response, e);
                }


                //if everything went well return a json object with redirect location
                return renderJsonSuccess(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students/" + studentId, "Scholarship Deleted!", response);

            }
        });


        //update an existing  scholarship / student link
        put("/admin/student_scholarships/:id", new AdminTemplateRoute() {
            @Override
            public Object handle(RequestDecorator request, Response response, VelocityContext templateParams) {
                //grab the id provided
                String scholarshipLinkId = request.params(":id");
                String studentId = null;

                try {
                    //pull in the daos
                    Dao<StudentScholarshipLinkData, Integer> studentScholarshipLinkDao = DBHelper.getStudentScholarshipLinkDao();

                    //retrieve the indicated record
                    StudentScholarshipLinkData scholarshipLink = studentScholarshipLinkDao.queryForId(Integer.parseInt(scholarshipLinkId));

                    //get the student and id
                    studentId = scholarshipLink.getStudent().getId() + "";

                    //update the fields
                    scholarshipLink.setStatus(request.queryParams("status"));
                    scholarshipLink.setSponsorName(request.queryParams("sponsorName"));

                    //update this record
                    studentScholarshipLinkDao.update(scholarshipLink);

                } catch (Exception e) {
                    return renderJsonException(response, e);
                }
                //if everything went well return a json object with redirect location
                return renderJsonSuccess(App.CONFIGURATION.getServerConfig().getApplicationPath() + "/admin/students/" + studentId, "Scholarship Updated!", response);
            }
        });

    }



    /**
     * Populates a Student object from the Request
     * @param student
     * @param request
     * @throws SQLException
     */
    private void populateStudentFromRequest(StudentData student, RequestDecorator request) throws SQLException {
        Dao<StudentData, Integer> studentsDao = DBHelper.getStudentDao();
        Dao<SchoolLevelData, Integer> schoolLevelDao = DBHelper.getSchoolLevelDao();
        Dao<CommunityData, Integer> communityDao = DBHelper.getCommunityDao();
        int schoolLevelId = Integer.parseInt(request.queryParams("schoolLevel"));
        int communityId = Integer.parseInt(request.queryParams("community"));

        student.setFirstName(request.queryParams("firstName"));
        student.setLastName(request.queryParams("lastName"));
        student.setKnownAs(request.queryParams("knownAs"));
        student.setGender(request.queryParams("gender"));
        student.setBirthDate(new DateTranformer().parse(request.queryParams("birthDate")));
        student.setDateCreated(new Date());
        student.setImagePath(request.queryParams("imagePath"));
        student.setGrade(Integer.parseInt(request.queryParams("grade")));
        student.setBiography(request.queryParams("biography"));
        student.setFutureAspirations(request.queryParams("futureAspirations"));
        student.setMiscNotes(request.queryParams("miscNotes"));
        student.setFullySponsored(request.queryParams("fullySponsored"));
        student.setFavoriteSubject(request.queryParams("favoriteSubject"));
        student.setEtapestryAccountNumber(Integer.parseInt(request.queryParams("etapestryAccountNumber")));
        student.setSponsorYear(Integer.parseInt(request.queryParams("sponsorYear")));
        student.setSchoolLevel(schoolLevelDao.queryForId(schoolLevelId));
        student.setCommunity(communityDao.queryForId(communityId));
        student.setRegion(request.queryParams("region"));
    }
    /**
     * Generates all of the options fields needed
     * @return
     * @throws SQLException
     */
    private Map<String, OptionsProvider> getEditOrCreateStudentOptionParams() throws SQLException {

        //need to pull in all the communities, school types, and scholarship types
        Dao<CommunityData, Integer> communityDao = DBHelper.getCommunityDao();
        Dao<SchoolLevelData, Integer> schoolLevelDao = DBHelper.getSchoolLevelDao();
        Dao<ScholarshipTypeData, Integer>  scholarshipTypesDao = DBHelper.getScholarshipTypeDao();

        List<AbstractMap.SimpleEntry<String, String>> fullySponsoredOptions = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
        fullySponsoredOptions.add(new AbstractMap.SimpleEntry<String, String>("y", "Yes"));
        fullySponsoredOptions.add(new AbstractMap.SimpleEntry<String, String>("n", "No"));
        List<AbstractMap.SimpleEntry<String, String>> genderOptions = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
        genderOptions.add(new AbstractMap.SimpleEntry<String, String>("f", "Female"));
        genderOptions.add(new AbstractMap.SimpleEntry<String, String>("m", "Male"));
        List<AbstractMap.SimpleEntry<String, String>> regionOptions = new ArrayList<AbstractMap.SimpleEntry<String, String>>();
        regionOptions.add(new AbstractMap.SimpleEntry<String, String>("us", "United States"));
        regionOptions.add(new AbstractMap.SimpleEntry<String, String>("ca", "Canada"));
        regionOptions.add(new AbstractMap.SimpleEntry<String, String>("--", "All"));
        String[] gradeOptions = new String[]{"7", "8", "9", "10", "11", "12"};

        //map select field name -> options
        Map<String, OptionsProvider> optionsProviderMap = new HashMap<String, OptionsProvider>();

        optionsProviderMap.put("schoolLevel", new DaoOptionProvider<SchoolLevelData>(schoolLevelDao.queryForAll()) {
            @Override
            public AbstractMap.SimpleEntry<String, String> convertObjectToOption(SchoolLevelData obj){
                return new AbstractMap.SimpleEntry<String, String>(obj.getId() + "", obj.getName());
            }
        });

        optionsProviderMap.put("community", new DaoOptionProvider<CommunityData>(communityDao.queryForAll()){
            @Override
            public AbstractMap.SimpleEntry<String, String> convertObjectToOption(CommunityData obj){
                return new AbstractMap.SimpleEntry<String, String>(obj.getId() + "", obj.getName());
            }
        });
        optionsProviderMap.put("scholarshipTypes", new DaoOptionProvider<ScholarshipTypeData>(scholarshipTypesDao.queryForAll()) {
            @Override
            public AbstractMap.SimpleEntry<String, String> convertObjectToOption(ScholarshipTypeData obj){
                return new AbstractMap.SimpleEntry<String, String>(obj.getId() + "", obj.getName());
            }
        });

        optionsProviderMap.put("gender", new SimpleOptionProvider(genderOptions));
        optionsProviderMap.put("region", new SimpleOptionProvider(regionOptions));
        optionsProviderMap.put("grade", new SimpleOptionProvider(gradeOptions));
        optionsProviderMap.put("fullySponsored", new SimpleOptionProvider(fullySponsoredOptions));

        return optionsProviderMap;

    }
	
	//responsible for rendering error page
	private String renderException(Response response, VelocityContext templateParams, Exception e) {
			templateParams.put("page_title", "Internal Error");
			templateParams.put("exception", ErrorUtility.getStackTraceAsString(e));
			StringWriter writer = new StringWriter();
			try{
                Velocity.getTemplate(Constants.ADMIN_ERROR_TEMPLATE_PATH).merge(templateParams, writer);
				response.status(200);
			}catch(Exception e1){
				response.status(500);
			}
			return writer.toString();
	}

    /**
     * redirects the provide URL with the given message
     * @param url
     * @param message
     * @return
     */
    private void redirectWithMessage(Response response, String url, String message){
        if(message != null){
            try {
                message = URLEncoder.encode(message,"UTF-8");
            } catch (UnsupportedEncodingException e) {
                message = URLEncoder.encode(message);
            }
            message = "?userMessage=" + message;
        }else{
            message = "";
        }

        response.redirect(url + message);
    }

    /**
     *
     *
     * @param redirectTo
     * @return
     */
    private Object renderJsonSuccess(String redirectTo, String userMessage, Response response){
        //first process the message for the user
        if(userMessage != null){
            try {
                userMessage = URLEncoder.encode(userMessage, "UTF-8");
            } catch (UnsupportedEncodingException e) {
                userMessage = URLEncoder.encode(userMessage);
            }

            userMessage = "?userMessage=" + userMessage;
        } else{
            userMessage = "";
        }

        JsonObject resp = new JsonObject();
        resp.add("status", new JsonPrimitive("success"));
        resp.add("redirectTo", new JsonPrimitive(redirectTo + userMessage));
        return Utils.sendJsonRespsonse(resp, response);
    }

	//responsible for rendering error page
	private String renderJsonException(Response response, Exception e) {
        JsonObject resp = new JsonObject();
        resp.add("status", new JsonPrimitive("error"));
        resp.add("error", new JsonPrimitive(ErrorUtility.getStackTraceAsString(e)));
        return Utils.sendJsonRespsonse(resp, response);
	}

    private String renderJsonException(Response response, String errorMsg) {
        JsonObject resp = new JsonObject();
        resp.add("status", new JsonPrimitive("error"));
        resp.add("error", new JsonPrimitive(errorMsg));
        return Utils.sendJsonRespsonse(resp, response);
	}


    private  String hash(byte[] input) throws NoSuchAlgorithmException {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            md.reset();
            md.update(input);

            return byteToHex(md.digest());
        } catch (NoSuchAlgorithmException e) {
            throw e;
        }
    }
    private String byteToHex(final byte[] hash) {
        Formatter formatter = new Formatter();
        for (byte b : hash)
        {
            formatter.format("%02x", b);
        }
        String result = formatter.toString();
        formatter.close();
        return result;
    }
}
