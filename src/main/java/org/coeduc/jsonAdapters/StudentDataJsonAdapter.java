/**
 * 
 */
package org.coeduc.jsonAdapters;

import java.io.IOException;

import org.coeduc.dao.StudentData;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

/**
 * @author calebweeks
 *
 */
public class StudentDataJsonAdapter extends TypeAdapter<StudentData> {

	/* (non-Javadoc)
	 * @see com.google.gson.TypeAdapter#write(com.google.gson.stream.JsonWriter, java.lang.Object)
	 */
	@Override
	public void write(JsonWriter out, StudentData value) throws IOException {
		out.beginObject();
		
		
		out.endObject();
	}

	/* (non-Javadoc)
	 * @see com.google.gson.TypeAdapter#read(com.google.gson.stream.JsonReader)
	 */
	@Override
	public StudentData read(JsonReader in) throws IOException {
		return null;
	}

}
