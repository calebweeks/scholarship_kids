/**
 * 
 */
package org.coeduc.jsonAdapters;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * @author calebweeks
 *
 */
public class ScholarshipLinkExclusionStrategy implements ExclusionStrategy{

	/* (non-Javadoc)
	 * @see com.google.gson.ExclusionStrategy#shouldSkipField(com.google.gson.FieldAttributes)
	 */
	public boolean shouldSkipField(FieldAttributes f) {
		return f.getAnnotation(JsonExclusion.class) != null;
	}

	/* (non-Javadoc)
	 * @see com.google.gson.ExclusionStrategy#shouldSkipClass(java.lang.Class)
	 */
	public boolean shouldSkipClass(Class<?> clazz) {
		return false;
	}

}
