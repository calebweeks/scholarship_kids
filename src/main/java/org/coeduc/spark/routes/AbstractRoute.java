/**
 * 
 */
package org.coeduc.spark.routes;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.coeduc.spark.decorators.PathInfo;
import org.coeduc.spark.decorators.RequestDecorator;

import spark.Request;
import spark.Response;
import spark.Route;

/**
 * @author calebweeks
 *
 */
public abstract class AbstractRoute implements Route{

	private String contentType;


    public AbstractRoute(){}
	public AbstractRoute(String contentType) {
		this.contentType = contentType;
	}


	/* (non-Javadoc)
	 * @see spark.Route#handle(spark.Request, spark.Response)
	 */
	@Override
	public Object handle(Request request, Response response) {
        if(StringUtils.isNotEmpty(contentType)){
            response.raw().setContentType(contentType);
        }

        response.status(200);
		return handle(new RequestDecorator(request), response, getDefaultTemplateParams(request));
	}
	
	protected abstract VelocityContext getDefaultTemplateParams(Request request);
	public abstract Object handle(RequestDecorator request, Response response, VelocityContext templateParams);/**
     * Retrieves the given template
     * @param templatePath
     * @return
     */
    protected Template getTemplate(String templatePath) {
        return Velocity.getTemplate(templatePath);
    }



}
