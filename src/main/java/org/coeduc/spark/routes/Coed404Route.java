/**
 * 
 */
package org.coeduc.spark.routes;

import org.apache.commons.lang3.StringUtils;
import org.coeduc.spark.decorators.RequestDecorator;

import spark.Filter;
import spark.Request;
import spark.Response;

/**
 * @author calebweeks
 *
 */
public class Coed404Route implements Filter {


	/* (non-Javadoc)
	 * @see spark.Filter#handle(spark.Request, spark.Response)
	 */
	@Override
	public void handle(Request request, Response response) {
		
		//missing item - we need to check for 0 as well since Spark doesn't set the 
		//status of the response until after Filter executions
		if(response.raw().getStatus() == 404 || response.raw().getStatus() == 0){
			RequestDecorator req = new RequestDecorator(request);
			
			//determine if this was likely a request for an html page
			boolean isPageReq = request.pathInfo().endsWith("/") || request.pathInfo().endsWith(".html") || StringUtils.isEmpty(req.getPathInfo().getExtension());
			boolean isAppResReq = request.pathInfo().startsWith("/scholarship_kids_public");
			//if it is a page request, then just redirect it to the landing page
			if(isPageReq){
				response.redirect("/");
			}else if(!isAppResReq && !"coeduc.org".equals(request.host())){
				//otherwise, if we're not in the live site, redirect request for res
				//assuming it's not obviously a request for one of the app resources
				response.redirect(request.raw().getRequestURL().toString().replace(request.host(), "coeduc.org"));
			}else{
				//otherwise, send the 404
				response.type("text/html");
			}
		}
		
		
	}

}
