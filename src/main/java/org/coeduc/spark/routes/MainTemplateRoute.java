/**
 * 
 */
package org.coeduc.spark.routes;

import org.apache.velocity.VelocityContext;
import org.coeduc.App;
import org.coeduc.StaticIncludeManager;
import org.coeduc.dao.DBHelper;
import org.coeduc.dao.RegionData;
import org.coeduc.spark.decorators.RequestDecorator;

import spark.Request;
import spark.Response;

import java.sql.SQLException;

/**
 * @author calebweeks
 *
 */
public abstract class MainTemplateRoute extends AbstractRoute{
	
	private StaticIncludeManager includeManager;
	

	public MainTemplateRoute(String contentType, StaticIncludeManager includeManager) {
		super(contentType);
		this.includeManager = includeManager; 
	}



	public MainTemplateRoute(StaticIncludeManager includeManager) {
		this.includeManager = includeManager;
	}


	
	public abstract Object handle(RequestDecorator request, Response response, VelocityContext templateParams);
	
	/**
	 * Simply initializes a Map and populates the fields that 
	 * need to be present in every template
	 * @return
	 */
	protected VelocityContext getDefaultTemplateParams(Request request){
		//initialize our templateParams and populate with default items
		String region = getRegion(request);
		if("error".equals(region)){
			region = "us";
		}
		VelocityContext templateParams = new VelocityContext();
		templateParams.put("headerContent", includeManager.getHeaderContent());
		templateParams.put("navigationContent", includeManager.getNavigationContent());
		templateParams.put("footerContent", includeManager.getFooterContent());
		templateParams.put("applicationPath", App.CONFIGURATION.getServerConfig().getApplicationPath());
		templateParams.put("imageNotFoundPath", "scholarship_kids_public/images/image-not-found.gif");
		templateParams.put("dollarSign", "$"); //escaping characters is absurdly difficult in Velocity. Just include as var
		templateParams.put("language", request.params("language"));
		templateParams.put("region", region);
		try {
			RegionData requestRegion = DBHelper.getRegionDao().queryForEq("id", region).get(0);
			templateParams.put("requestRegion", requestRegion);
		} catch (SQLException e) {}

		return templateParams; 
	}

	public String getRegion(Request request){
		String uri = request.raw().getRequestURI();
		return uri.split("/")[1];
	}
	
	
}
