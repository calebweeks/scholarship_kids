/**
 * 
 */
package org.coeduc.spark.routes;

import org.apache.velocity.VelocityContext;
import org.coeduc.App;
import org.coeduc.spark.decorators.RequestDecorator;
import spark.Request;
import spark.Response;

import java.util.Date;

/**
 * @author calebweeks
 *
 */
public abstract class AdminTemplateRoute extends AbstractRoute{

	/**
	 * Automatically sets a content type of 'text/html'
	 */
	public AdminTemplateRoute() {
		super("text/html");
	}


	/**
	 * Simply initializes a Map and populates the fields that 
	 * need to be present in every template
	 * @returnO
	 */
	protected VelocityContext getDefaultTemplateParams(Request request){
		//initialize our templateParams and populate with default items
		VelocityContext templateParams = new VelocityContext();
		templateParams.put("applicationPath", App.CONFIGURATION.getServerConfig().getApplicationPath());
		templateParams.put("now", (new Date()).getTime());
		templateParams.put("dollarSign", "$"); //escaping characters is absurdly difficult in Velocity. Just include as var
		
		return templateParams; 
	}


    public abstract Object handle(RequestDecorator request, Response response, VelocityContext templateParams);

}
