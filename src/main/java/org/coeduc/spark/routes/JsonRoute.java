/**
 * 
 */
package org.coeduc.spark.routes;

import org.coeduc.spark.decorators.RequestDecorator;

import spark.Request;
import spark.Response;
import spark.Route;

/**
 * @author calebweeks
 *
 */
public abstract class JsonRoute implements Route {


	/* (non-Javadoc)
	 * @see spark.Route#handle(spark.Request, spark.Response)
	 */
	@Override
	public Object handle(Request request, Response response) {
		response.raw().setContentType("application/json; charset=utf-8");
		response.status(200);
		return handleRequest(new RequestDecorator(request), response);
	}
	
	public abstract Object handleRequest(RequestDecorator request, Response response);

}
