/**
 * 
 */
package org.coeduc.spark.decorators;

/**
 * This class simply encapsulates the Spark path info with some
 * additional information
 * @author calebweeks
 *
 */
public class PathInfo {
	private String sparkRequestPathInfo; 
	private String extension;
	private String path; 
	
	
	public PathInfo(String sparkRequestPathInfo){
		this.sparkRequestPathInfo = sparkRequestPathInfo; 
		
		//analyze the path info for path and extension
		if(sparkRequestPathInfo != null){
			int indexOfLastSlash = sparkRequestPathInfo.lastIndexOf('/');
			if(indexOfLastSlash > -1){
				//did the request end in a slash?
				if(indexOfLastSlash == sparkRequestPathInfo.length() - 1){
					path = indexOfLastSlash == 0 ? "/" : sparkRequestPathInfo.substring(0, indexOfLastSlash); 
				}else{
					int indexOfLastDot = sparkRequestPathInfo.lastIndexOf('.');
					//check to see if we found a dot - this indicates an extension
					if(indexOfLastDot > -1 && indexOfLastDot > indexOfLastSlash){
						extension = indexOfLastDot < sparkRequestPathInfo.length() - 1? sparkRequestPathInfo.substring(indexOfLastDot + 1) : null;
						path = sparkRequestPathInfo.substring(0, indexOfLastDot);
					}else{
						//no extension means the path is simply the path info
						path = sparkRequestPathInfo; 
					}
				}
			}else{
				//no slash means path should be an empty string
				path = ""; 
				
			}
			
		}
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @return the path
	 */
	public String getPath() {
		return path;
	}
	

}
