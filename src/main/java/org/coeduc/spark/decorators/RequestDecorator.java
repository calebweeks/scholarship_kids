/**
 * 
 */
package org.coeduc.spark.decorators;

import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import spark.QueryParamsMap;
import spark.Request;
import spark.Session;

/**
 * @author calebweeks
 *
 */
public class RequestDecorator extends Request{
	private Request request; 
	private PathInfo pathInfo;
	public RequestDecorator(Request request){
		this.request = request;
		this.pathInfo = new PathInfo(request.pathInfo());
	}
	
	public PathInfo getPathInfo(){
		return pathInfo;
	}

	/* (non-Javadoc)
	 * @see spark.Request#attribute(java.lang.String, java.lang.Object)
	 */
	@Override
	public void attribute(String attribute, Object value) {
		// TODO Auto-generated method stub
		request.attribute(attribute, value);
	}

	/* (non-Javadoc)
	 * @see spark.Request#attribute(java.lang.String)
	 */
	@Override
	public Object attribute(String attribute) {
		// TODO Auto-generated method stub
		return request.attribute(attribute);
	}

	/* (non-Javadoc)
	 * @see spark.Request#attributes()
	 */
	@Override
	public Set<String> attributes() {
		// TODO Auto-generated method stub
		return request.attributes();
	}

	/* (non-Javadoc)
	 * @see spark.Request#body()
	 */
	@Override
	public String body() {
		// TODO Auto-generated method stub
		return request.body();
	}

	/* (non-Javadoc)
	 * @see spark.Request#contentLength()
	 */
	@Override
	public int contentLength() {
		// TODO Auto-generated method stub
		return request.contentLength();
	}

	/* (non-Javadoc)
	 * @see spark.Request#contentType()
	 */
	@Override
	public String contentType() {
		// TODO Auto-generated method stub
		return request.contentType();
	}

	/* (non-Javadoc)
	 * @see spark.Request#headers()
	 */
	@Override
	public Set<String> headers() {
		// TODO Auto-generated method stub
		return request.headers();
	}

	/* (non-Javadoc)
	 * @see spark.Request#headers(java.lang.String)
	 */
	@Override
	public String headers(String header) {
		// TODO Auto-generated method stub
		return request.headers(header);
	}

	/* (non-Javadoc)
	 * @see spark.Request#host()
	 */
	@Override
	public String host() {
		// TODO Auto-generated method stub
		return request.host();
	}

	/* (non-Javadoc)
	 * @see spark.Request#ip()
	 */
	@Override
	public String ip() {
		// TODO Auto-generated method stub
		return request.ip();
	}

	/* (non-Javadoc)
	 * @see spark.Request#params(java.lang.String)
	 */
	@Override
	public String params(String param) {
		// TODO Auto-generated method stub
		return request.params(param);
	}

	/* (non-Javadoc)
	 * @see spark.Request#pathInfo()
	 */
	@Override
	public String pathInfo() {
		// TODO Auto-generated method stub
		return request.pathInfo();
	}

	/* (non-Javadoc)
	 * @see spark.Request#port()
	 */
	@Override
	public int port() {
		// TODO Auto-generated method stub
		return request.port();
	}

	/* (non-Javadoc)
	 * @see spark.Request#queryMap()
	 */
	@Override
	public QueryParamsMap queryMap() {
		// TODO Auto-generated method stub
		return request.queryMap();
	}

	/* (non-Javadoc)
	 * @see spark.Request#queryMap(java.lang.String)
	 */
	@Override
	public QueryParamsMap queryMap(String key) {
		// TODO Auto-generated method stub
		return request.queryMap(key);
	}

	/* (non-Javadoc)
	 * @see spark.Request#queryParams()
	 */
	@Override
	public Set<String> queryParams() {
		// TODO Auto-generated method stub
		return request.queryParams();
	}

	/* (non-Javadoc)
	 * @see spark.Request#queryParams(java.lang.String)
	 */
	@Override
	public String queryParams(String queryParam) {
		// TODO Auto-generated method stub
		return request.queryParams(queryParam);
	}

	/* (non-Javadoc)
	 * @see spark.Request#queryString()
	 */
	@Override
	public String queryString() {
		// TODO Auto-generated method stub
		return request.queryString();
	}

	/* (non-Javadoc)
	 * @see spark.Request#raw()
	 */
	@Override
	public HttpServletRequest raw() {
		// TODO Auto-generated method stub
		return request.raw();
	}

	/* (non-Javadoc)
	 * @see spark.Request#requestMethod()
	 */
	@Override
	public String requestMethod() {
		// TODO Auto-generated method stub
		return request.requestMethod();
	}

	/* (non-Javadoc)
	 * @see spark.Request#scheme()
	 */
	@Override
	public String scheme() {
		// TODO Auto-generated method stub
		return request.scheme();
	}

	/* (non-Javadoc)
	 * @see spark.Request#session()
	 */
	@Override
	public Session session() {
		// TODO Auto-generated method stub
		return request.session();
	}

	/* (non-Javadoc)
	 * @see spark.Request#session(boolean)
	 */
	@Override
	public Session session(boolean arg0) {
		// TODO Auto-generated method stub
		return request.session(arg0);
	}

	/* (non-Javadoc)
	 * @see spark.Request#url()
	 */
	@Override
	public String url() {
		// TODO Auto-generated method stub
		return request.url();
	}

	/* (non-Javadoc)
	 * @see spark.Request#userAgent()
	 */
	@Override
	public String userAgent() {
		// TODO Auto-generated method stub
		return request.userAgent();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return request.equals(obj);
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return request.hashCode();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return request.toString();
	}
	
	
	
}
