/**
 * 
 */
package org.coeduc.logging;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.Filter;
import spark.Request;
import spark.Response;
import spark.Spark;

/**
 * @author calebweeks
 *
 */
public class RequestLogHelper {
	
	
	//instantiate our mapper of thread id to start time
	private static final Map<Long, Long> timeTracker = new HashMap<Long, Long>();
	
	//get the request.log Logger
	private static Logger requestLog = LoggerFactory.getLogger("requestLogger");
	
	
	/**
	 * Add a before filter to log request and start request
	 * time logging
	 */
	public static void initializeLogPreRequestFilter(){
		
		Spark.before(new Filter(){

			@Override
			public void handle(Request request, Response response) {
				//beforehand we log the store the start time of the request
				timeTracker.put(Thread.currentThread().getId(), System.currentTimeMillis());
				
				//and out general info about the nature of the request
				//it doesn't really matter what log level we use, as long
				//as it's INFO or above
				StringBuilder builder = new StringBuilder();
				builder.append("<- [")
				.append(request.raw().getRemoteAddr()).append("] ")
				.append(request.raw().getMethod()).append(" ")
				.append(request.raw().getRequestURI() != null ? request.raw().getRequestURI() : "");
				//spark actually logs this already as part of the thread id...but that's ok. I'm going to 
				//duplicate anyway
				if(request.raw().getQueryString() != null){
					builder.append("?").append(request.raw().getQueryString());
				}
				
				
				//actually log the message
				requestLog.info(builder.toString());
			}
			
		});
	}
	
	/**
	 * outputs the content type, length of req, and status code
	 */
	public  static void initializeLogPostRequestFilter(){
		Spark.after(new Filter(){

			@Override
			public void handle(Request request, Response response) {
				long fullTime = 0; 
				
				//grab our start time
				Long startTime = timeTracker.get(Thread.currentThread().getId());
				
				if(startTime != null){
					fullTime = System.currentTimeMillis() - startTime;
				}
				
				StringBuilder builder = new StringBuilder();
				builder.append("-> ").append(response.raw().getContentType())
				.append(" ").append(fullTime).append("ms ").append(response.raw().getStatus());
				requestLog.info(builder.toString());
			}
			
		});
	}

}
