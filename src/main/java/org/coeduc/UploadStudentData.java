/**
 * 
 */
package org.coeduc;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.coeduc.config.AppConfiguration;
import org.coeduc.dao.CommunityData;
import org.coeduc.dao.DBHelper;
import org.coeduc.dao.ScholarshipTypeData;
import org.coeduc.dao.SchoolLevelData;
import org.coeduc.dao.StudentData;
import org.coeduc.dao.StudentScholarshipLinkData;

import com.google.gson.JsonObject;
import com.google.gson.JsonPrimitive;
import com.j256.ormlite.dao.Dao;

/**
 * @author calebweeks
 *
 */
public class UploadStudentData {
	
	
	
	public static void main(String [] args) throws IOException{
		
		JsonObject config = new JsonObject();
		config.add("environment", new JsonPrimitive(""));
		JsonObject serverConfig = new JsonObject();
		serverConfig.add("port", new JsonPrimitive(7777));
		serverConfig.add("applicationPath", new JsonPrimitive(""));
		config.add("server", serverConfig);
		JsonObject dbConfig = new JsonObject();
		dbConfig.add("port", new JsonPrimitive(3306));
		/* LIVE CONFIG*/
//		dbConfig.add("username", new JsonPrimitive("bcca00d7d661ca"));
//		dbConfig.add("password", new JsonPrimitive("90c3bd19"));
//		dbConfig.add("host", new JsonPrimitive("us-cdbr-east-03.cleardb.com"));
//		dbConfig.add("database", new JsonPrimitive("heroku_f5dd7d2da83aa8b"));
		/* LOCAL CONFIG*/
		dbConfig.add("username", new JsonPrimitive("root"));
		dbConfig.add("password", new JsonPrimitive(""));
		dbConfig.add("host", new JsonPrimitive("localhost"));
		dbConfig.add("database", new JsonPrimitive("coeduc_scholarshipkids"));
		
		
		config.add("db", dbConfig);
		
		App.CONFIGURATION = new AppConfiguration(config);

		SimpleDateFormat janFirstBirthday = new SimpleDateFormat("mm/dd/yyyy");

		List<StudentData> students = new ArrayList<StudentData>();
		final Map<Integer, Integer> studentOrderMapping = new HashMap<Integer, Integer>();
		try {

            DBHelper.initialize();
			//setup all the DAO we'll need
			Dao<StudentData, Integer> dao = DBHelper.getStudentDao();
			Dao<ScholarshipTypeData, Integer> scholarshipTypeDao = DBHelper.getScholarshipTypeDao();
			Dao<CommunityData, Integer> communityDao = DBHelper.getCommunityDao();
			Dao<SchoolLevelData, Integer> schoolLevelDao = DBHelper.getSchoolLevelDao();
			Dao<StudentScholarshipLinkData, Integer> scholarshipLinksDao = DBHelper.getStudentScholarshipLinkDao();
			//read in csv file
			//Reader reader = new FileReader("/Users/caleb/Desktop/students.csv");
		
			CsvReader csvReader = new CsvReader("/Users/caleb/Desktop/students.csv", ',', Charset.forName("UTF-8"));


			while(csvReader.readRecord()){
				csvReader.getCurrentRecord();
//				for(int i = 0; i < csvReader.getColumnCount(); i++){
//					System.out.print(i + ": " + csvReader.get(i) + ", ");
//
//				}

				StudentData student = new StudentData();

				//setup all the easy data first
				student.setLastName(csvReader.get(0).trim());
				student.setFirstName(csvReader.get(1).trim());
				student.setKnownAs(csvReader.get(2).trim());
				student.setGender("male".equals(csvReader.get(3).trim().toLowerCase()) ? "m": "f");
				student.setBirthDate(janFirstBirthday.parse("01/01/" + (2017 - Integer.parseInt(csvReader.get(4)))));
				student.setImagePath(csvReader.get(5).trim());
				student.setGrade(Integer.parseInt(csvReader.get(6)));
				student.setBiography(csvReader.get(7).trim());
				student.setFutureAspirations(csvReader.get(8).trim());
				student.setFullySponsored("n");
				student.setFavoriteSubject(csvReader.get(10).trim());
				student.setEtapestryAccountNumber(Integer.parseInt(csvReader.get(11).trim()));
				student.setSponsorYear(2018);
				student.setRegion("--");


				//Now we pull in the values that are foreign keys
				String communityName = csvReader.get(15).trim();
				System.out.println("Community: " + communityName);
				student.setCommunity(communityDao.queryForEq("community_name", communityName).get(0));
				student.setSchoolLevel(schoolLevelDao.queryForEq("level_name", student.getGrade() > 9 ? "High School" : "Middle School").get(0));





				students.add(student);
//
//				studentOrderMapping.put(student.getEtapestryAccountNumber(), Integer.parseInt(csvReader.get(4).trim()));
			}

			//sort based on the order provided
//			Collections.sort(students, new Comparator<StudentData>(){
//
//				public int compare(StudentData o1, StudentData o2) {
//					return studentOrderMapping.get(o1.getEtapestryAccountNumber()).compareTo(studentOrderMapping.get(o2.getEtapestryAccountNumber()));
//				}
//
//			});

			for(StudentData student : students){
				dao.create(student);


				//add a new scholarship for this student
				//if they're not already fully funded
				if(!student.isFullySponsored()){
					StudentScholarshipLinkData scholarshipLink = new StudentScholarshipLinkData();
					scholarshipLink.setStatus("available");
					scholarshipLink.setStudent(student);
					scholarshipLink.setScholarship(scholarshipTypeDao.queryForEq("etapestry_value", "Diploma Scholarship").get(0));
					StudentScholarshipLinkData scholarshipLink2 = new StudentScholarshipLinkData();
					scholarshipLink2.setStatus("available");
					scholarshipLink2.setStudent(student);
					scholarshipLink2.setScholarship(scholarshipTypeDao.queryForEq("etapestry_value", "Honor Roll Scholarship").get(0));

					scholarshipLinksDao.create(scholarshipLink);
					scholarshipLinksDao.create(scholarshipLink2);
				}


				System.out.println(student);

			}

			String[] newCommunities = {
				"El Triunfo",
				"Yepocapa",
				"Santa Lucia",
				"Magdalena",
				"Santo Domingo"
			};

			for(String name : newCommunities){
				CommunityData communityData = new CommunityData();
				communityData.setName(name);

				communityDao.create(communityData);
			}

            ///current max ID is 681
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//
		
		System.out.println("---DONE---");
	}

}
