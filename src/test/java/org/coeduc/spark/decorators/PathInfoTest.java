/**
 * 
 */
package org.coeduc.spark.decorators;

import junit.framework.TestCase;

/**
 * @author calebweeks
 *
 */
public class PathInfoTest extends TestCase {
	private PathInfo noPath; 
	private PathInfo rootPath; 
	private PathInfo pathEndWithSlash;
	private PathInfo noExtension; 
	private PathInfo fullPathWithExt;
	private PathInfo fakeExt; 
	/* (non-Javadoc)
	 * @see junit.framework.TestCase#setUp()
	 */
	protected void setUp() throws Exception {
		super.setUp();
		noPath = new PathInfo("");
		rootPath = new PathInfo("/");
		pathEndWithSlash = new PathInfo("/some/path/");
		noExtension = new PathInfo("/some/path/noext");
		fullPathWithExt = new PathInfo("/some/path.html");
		fakeExt = new PathInfo("/some/path.");
	}

	/**
	 * Test method for {@link org.coeduc.spark.decorators.PathInfo#getExtension()}.
	 */
	public void testGetExtension() {
		assertTrue(noPath.getExtension() == null); 
		assertTrue(rootPath.getExtension() == null); 
		assertTrue(pathEndWithSlash.getExtension() == null); 
		assertTrue(noExtension.getExtension() == null); 
		assertTrue(fullPathWithExt.getExtension() != null); 
		assertTrue("html".equals(fullPathWithExt.getExtension())); 
		assertTrue(fakeExt.getExtension() == null); 
	}

	/**
	 * Test method for {@link org.coeduc.spark.decorators.PathInfo#getPath()}.
	 */
	public void testGetPath() {
		assertTrue("".equals(noPath.getPath())); 
		assertTrue("/".equals(rootPath.getPath())); 
		assertTrue("/some/path".equals(pathEndWithSlash.getPath())); 
		assertTrue("/some/path/noext".equals(noExtension.getPath())); 
		assertTrue("/some/path".equals(fullPathWithExt.getPath())); 
		assertTrue("/some/path".equals(fakeExt.getPath())); 
	}

}
